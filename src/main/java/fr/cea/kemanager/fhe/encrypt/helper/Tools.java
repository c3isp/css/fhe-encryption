package fr.cea.kemanager.fhe.encrypt.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fr.cea.kemanager.fhe.encrypt.repository.FheBlacklistAnalysisRespository;
import fr.cea.kemanager.fhe.encrypt.vault.KVEncryption;
import fr.cea.kemanager.fhe.encrypt.vault.KVKeyManager;
import fr.cea.kemanager.fhe.encrypt.vault.io.FileOperations;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.vault.common.VaultStart;

public class Tools {
	
	private static Logger log = LoggerFactory.getLogger(Tools.class);
	private final static String logStatus = "[TOOLS-FHE-CINGULATA] ";	

	/**
	 * *
	 * @param fileName
	 * @param suffix
	 * @param fileObjectContent
	 * @return File
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static File createTempFile(String fileName, String suffix, byte[] fileObjectContent)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path pathDirectory = Files.createTempDirectory(fileName);
		String filePath = pathDirectory + File.separator + fileName;
		FileOperations.PrintData(filePath, fileObjectContent);		        
        return new File(filePath);
	}
	
	public static File createTempFile(String fileName, String suffix, String fileObjectContent)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // writing sample data
        Files.write(path, fileObjectContent.getBytes(StandardCharsets.UTF_8));
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}    
	
	public static File createTempFile(String fileName, String suffix)
			throws IOException, FileNotFoundException {
		// Since Java 1.7 Files and Path API simplify operations on files
		Path path = Files.createTempFile(fileName, suffix);
        File file = path.toFile();
        // This tells JVM to delete the file on JVM exit.
        // Useful for temporary files in tests.
        file.deleteOnExit();
        return file;
	}
	
	public static ResponseEntity<DataObject> decryptDataWithKreyvium(
			@NotNull String fieldToDecrypt, 
			@NotNull String offsetValues,
			@NotNull String dsaId, 
			@NotNull String dataContent, 
			@NotNull String TokenRoot) throws IOException 
	{
			List<String> txtEncryptedValue 	= ExtractValueFromFieldText(dataContent, fieldToDecrypt);
			Map<String, String> decryptedTextValue = __kreyviumDecryption(txtEncryptedValue, dsaId, offsetValues, dataContent, TokenRoot);
			if(decryptedTextValue.size() != 2) throw new IOException("Error during decrytion specific field with kreyvium !");
			String decryptedDataContent = decryptedTextValue.get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);						

			DataObject dataObject = new DataObject()
					.dataContent(decryptedDataContent)
					.dataName(dsaId + "_decrypted")
					.urlToBack("http_url_to_back");
			dataObject.setChecksum(
					Objects.hash(dataObject.getDataContent())+"");
//			if(fileData.getOtherProperties() != null && fileData.getOtherProperties().size() > 0)
//			{
//				dataObject.getOtherProperties().put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, 
//						fileData.getOtherProperties().get(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM));
//			}
			return new ResponseEntity<DataObject>(dataObject, HttpStatus.OK);
	}



	public static ResponseEntity<DataObject> applyingTranscipheringProcess(
			@NotNull DataObject 	fileData, 
			@NotNull String 		dsaId, 
			@NotNull String 		fieldToEncrypt, 
			@NotNull String 		TokenRoot) throws IOException 
	{		
		List<String> txtValueToEncrypt 	= ExtractValueFromFieldText(fileData.getDataContent(), fieldToEncrypt);
		Map<String, String> encryptedTextValue  = __kreyviumEncryption(txtValueToEncrypt, dsaId, fileData.getDataContent(), TokenRoot);
		if(encryptedTextValue.size() != 2) throw new IOException("Error during encrytion specific field with kreyvium !");
		fileData.setDataContent(encryptedTextValue.get(Constants.DefaultParam.CONTENT_FILE_PARAM));
		fileData.putOtherPropertiesItem(
				Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, 
				encryptedTextValue.get(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM));
		
		return new ResponseEntity<DataObject>(fileData, HttpStatus.OK);
	}

	
	/**
	 * ctiFile contains some information like this : dst=1:54.213.116.149
	 * @param fieldToEncrypt
	 * @param ctiFile
	 * @return
	 */
	public static List<String> ExtractValueFromFieldText(
			boolean usingKreyvium,
			@NotNull String dataContent, 
			@NotNull String fieldToEncrypt) 
	{
		String[] actualContent = dataContent.split("\\|");
		List<String> _lstStrIp = new ArrayList<String>();
		for (String s : actualContent) {			
			String[] parts = s.split(" ");
			for (int i=0; i<parts.length;i++) {
				if(parts[i].contains(fieldToEncrypt+"=") == false)
					continue;
								
				String motifEnryptedIp = parts[i].replaceAll(fieldToEncrypt+"=", ""); 
				if(usingKreyvium)
				{
					//Extract from encrypted field with Kreyvium 
					if(motifEnryptedIp.contains(":")) {
						_lstStrIp.add(motifEnryptedIp.split(":")[1]);					
					}
				}
				else 
					_lstStrIp.add(motifEnryptedIp);
			}
		}
		return _lstStrIp;
	}
	
	public static List<String> ExtractValueFromFieldText(
			@NotNull String dataContent, 
			@NotNull String fieldToEncrypt) 
	{		
		return ExtractValueFromFieldText(false, dataContent, fieldToEncrypt);
	}

	private static String __replaceEncryptedValueFromFieldText(
			@NotNull String fileDataString, 
			@NotNull String txtValueToEncrypt,
			@NotNull String strNewValue) 
	{
		String result = fileDataString;
		result = result.replaceFirst(txtValueToEncrypt, strNewValue);
		return result;
	}

	public static Map<String, String> __kreyviumEncryption(			
			List<String> 		txtValueToEncrypt, 
			@NotNull String 	dsAId, 
			@NotNull String 	fullText, 
			@NotNull String 	tokenRoot) 
	{
		KVEncryption kVEncrypt = new KVEncryption(new KVKeyManager(new VaultStart(tokenRoot).GetTemplate()));
		String _encryptedIPparts;
		String curr = fullText;
		for (String s : txtValueToEncrypt) {
			_encryptedIPparts = kVEncrypt.EncryptIPKV(s, dsAId);
			curr = __replaceEncryptedValueFromFieldText(curr, s, _encryptedIPparts);
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constants.DefaultParam.CONTENT_FILE_PARAM, curr);
		map.put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, "0");
		return map;
	}


	private static Map<String, String> __kreyviumDecryption(			
			List<String> txtValueToDecrypt, 
			@NotNull String dsaId,
			@NotNull String offsetValues, 
			@NotNull String fullText, 
			@NotNull String tokenRoot) 
	{		
		KVEncryption kVEncrypt = new KVEncryption(new KVKeyManager(new VaultStart(tokenRoot).GetTemplate()));
		String _decryptedIPparts;
		String curr = fullText;
		for (String s : txtValueToDecrypt) {
			_decryptedIPparts = kVEncrypt.DecryptIPKV(s, dsaId);
			curr = __replaceEncryptedValueFromFieldText(curr, s, _decryptedIPparts);
		}
		HashMap<String, String> map = new HashMap<String, String>();
		map.put(Constants.DefaultParam.CONTENT_FILE_PARAM, curr);
		map.put(Constants.DefaultParam.OFFSET_KEYSTREAM_PARAM, "0");
		return map;
	}
	

    public static String getChecksumFromByte(byte[] ori) {
		MessageDigest md=null;
		try {
			md = MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		md.update(ori, 0, ori.length);
		byte[] mdbytes = md.digest();
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	public static ResponseEntity<String> InvokeIAIBlacklistAnalysis(
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController,
			String dsAId, 
			String requestId, 
			String strUrlBack, 
			String fheModel)
	{					
		try {
			String dataIpStorageByRequestID 
						= String.format( Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsAId + File.separator + requestId, fheModel);
			File folder = new File(dataIpStorageByRequestID);	
			log.info(logStatus + "Seeking Data location " + dataIpStorageByRequestID);
			if(folder.exists() == false)
			{
				String message  = logStatus + "Data request " + requestId + " is not existed !" + dataIpStorageByRequestID;
				log.info(message);
				return new ResponseEntity<String>(message, HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
			}
			if(folder.isDirectory() == true)
			{				
				/*For each Request folder, loading all IP Folders*/
				for (final File fileEntry : folder.listFiles()) {					
					/*For each IP folder, loading all cipherTexts */	
					if(fileEntry.isDirectory() == true)
					{
						String fileLock = fileEntry.getAbsolutePath() + File.separator + Constants.LOCK_FILE;
						if (FileOperations.IsExisted(fileLock)) 
							throw new IOException("Existed File Lock, please waiting a moment !");
						
						DataObject baObj = new DataObject()
									.dataContent("")
									.checksum("Empty")
									.urlToBack(strUrlBack)
									.dataName(requestId.length() == 0 ? UUID.randomUUID().toString() : requestId);
						
						for (final File fileCipherText : fileEntry.listFiles()) {								
							if(fileCipherText.isDirectory() == true) continue;
							if(fileCipherText.getName().contains(".ct") == false) continue; 
							String contentBase64 = Base64.getEncoder().encodeToString(
									FileOperations.GetData(fileCipherText.getAbsolutePath()));
							baObj.getOtherProperties().put(fileCipherText.getName(), contentBase64);							
							log.debug(logStatus + " Loading file " + fileCipherText.getName() + " length = " + contentBase64.length());
						}					
						/*Send encrypted IP to FHE Analysis*/
						try {
							log.info(logStatus + " Sending object containing " + baObj.getOtherProperties().size() + " files");
							assert(fheBlacklistAnalyisiController != null);
							ResponseEntity<String> response = fheBlacklistAnalyisiController.invokeBlacklistAnalysis(baObj);
							log.info(logStatus + "Received response from IAI " + response.getBody());
							return response;
						} catch (NullPointerException | IOException e) {								
							log.info(logStatus + " Error Sending request for analysis IP from request " + baObj.getDataName()+ 
									"\nError: ");
							//e.printStackTrace();
						}
					}					
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.info(logStatus + "out of scope for response from IAI");
		}
		log.info(logStatus + "out of scope for response from IAI");
		return new ResponseEntity<String>("check in console", HttpStatus.BANDWIDTH_LIMIT_EXCEEDED);
	}
	
	public static String formatVDLPath(String pathToVDL) {
		
		String fileSystemPattern = "file://";
		String hdfsPattern = "hdfs://";
		
		if(pathToVDL.contains(fileSystemPattern))
			return pathToVDL.replace(fileSystemPattern, "");
		else if(pathToVDL.contains(hdfsPattern))
			return pathToVDL.replace(hdfsPattern, "");
		
		return pathToVDL;
	}
}
