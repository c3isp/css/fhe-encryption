package fr.cea.kemanager.fhe.encrypt.helper;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.cea.kemanager.fhe.encrypt.models.CipherText;
import fr.cea.kemanager.fhe.encrypt.models.ResultEncryptedData;
import fr.cea.kemanager.fhe.encrypt.vault.io.FileOperations;

public class ToolsFolder {
	private static final Logger log = LoggerFactory.getLogger(ToolsFolder.class);

	private static String m_threadName = "[FHE - ToolsFolder]: ";
	
	public static String CreateAllFolderNames(String storagePath) throws SecurityException, IOException  {
		//System.out.println("Process to create folder for < " + storagePath + " >");
		File file = new File(storagePath);
		if (!file.exists()) {
			if(file.mkdirs())
			{
				log.info(m_threadName + "Create user directory for FHE encryption purpose : " + storagePath);
			}
			else	
			{					
				System.out.println("Impossible to create user directory " + storagePath + " at " + file.getAbsolutePath());
				return null;
			}			
		}
		return file.getAbsolutePath();
	}	
	
	public void accept(Path name, ResultEncryptedData resEnData) {
    	File encrBitFile = name.toFile();					
		log.info("looking file " + encrBitFile.getName()); 				
		if(encrBitFile.isFile())
		{
			try {
				resEnData.appendChipherText(
						new CipherText()
							.content(FileUtils.readFileToByteArray(encrBitFile))
							.fileName(encrBitFile.getName()));
			} catch (IOException e) {
				e.printStackTrace();						
			}
		}
    }

	public static ResultEncryptedData getAllContentFilesInFolder(String storageEncryptedIPPath) {
		/* For the directory new File("/opt/a/b/c/"); -> RETURN folder /opt/a/b
		 * the method file.list() will give c
		 */
		ResultEncryptedData resEnData = new ResultEncryptedData();
		
		if(new File(storageEncryptedIPPath).exists() == false)
			return resEnData;
		
		Consumer<Path> saveFileContent= new Consumer<Path>() {
		    public void accept(Path name) {
		    	File encrBitFile = name.toFile();					
		    	if(encrBitFile.isFile())
					try {
						resEnData.appendChipherText(
								new CipherText()
									.content(FileUtils.readFileToByteArray(encrBitFile))
									.fileName(encrBitFile.getName()));
					} catch (IOException e) {
						e.printStackTrace();						
					}
		    }
		};
		
		try (Stream<Path> paths = Files.walk(Paths.get(storageEncryptedIPPath))) {
		    paths
		        .filter(Files::isRegularFile)
		        .forEach(saveFileContent);
		}
		catch (java.nio.file.NoSuchFileException e) {
			return null;	
		}
		catch (IOException e) {
			e.printStackTrace();			
		}

		log.info("get content of "+ resEnData.getResultAnalyse().size()+" files at " + storageEncryptedIPPath);
		return resEnData; 
	}	
	
	public static List<String> getAllChildFolderInFolderParent(String parentFolder) {
		/* For the directory new File("/opt/a/b/c/"); -> RETURN folder /opt/a/b
		 * the method file.list() will give c
		 */
		List<String> resultFolderData = new ArrayList<String>();
		
		if(new File(parentFolder).exists() == false)
			return resultFolderData;
		
		log.info("Looking parent folder " + parentFolder);		
		File fileParentFolder = new File(parentFolder);
		for(String childFolder :  fileParentFolder.list()) {
			log.info("Child Folder = " + childFolder);
			
			File encrBitFile = new File(String.format("%s%s%s", parentFolder, File.separator, childFolder));
			
	    	//log.info("[FHE-CINGULATA-MANAGER] + Looking Full folder " + encrBitFile.getAbsolutePath());
	    	if(encrBitFile.isDirectory())
	    	{
	    		//log.info("[FHE-CINGULATA-MANAGER] + Adding folder " + encrBitFile.getAbsolutePath());
				resultFolderData.add(encrBitFile.getAbsolutePath());
	    	}
		}		
		return resultFolderData; 
	}	
	
	
	public static ResultEncryptedData getPartialContentFilesInFolder(String storageEncryptedIPPath) {
		ResultEncryptedData resEnData = getAllContentFilesInFolder(storageEncryptedIPPath);
		for(CipherText ct : resEnData.getResultAnalyse())
		{			
			int lengthReprensetative = 32;
			byte[] partialContent = java.util.Arrays.copyOf(ct.getContent(), lengthReprensetative);
			ct.setContent(partialContent);
		}
		return resEnData;
	}

	public static String zipAllFilesInFolder(String folderIP, String requestId, Path destinationFolder) {		
		List<File> dataIPsFile = new ArrayList<File>();
		
		Consumer<Path> saveFileContent= new Consumer<Path>() {
		    public void accept(Path name) {
		    	File encrBitFile = name.toFile();					
		    	if(encrBitFile.isFile()) {		    		
		    		dataIPsFile.add(encrBitFile);
		    	}
		    }
		};		
		
		try (Stream<Path> paths = Files.walk(Paths.get(folderIP))) {
		    paths
		        .filter(Files::isRegularFile)
		        .forEach(saveFileContent);
		}
		catch (java.nio.file.NoSuchFileException e) {
			return null;	
		}
		catch (IOException e) {
			e.printStackTrace();			
		}
		log.info("Adding " + dataIPsFile.size() + " Ciphertexts for starting ZIP file");					
		File fileZip = null;
		try {
			fileZip = ZipUtility.zipFile(dataIPsFile, requestId);
			File destFile = new File(destinationFolder.toUri());
			log.info("fileZip path " + fileZip.getAbsolutePath() + " ready to move " + destFile.getAbsolutePath());
			FileOperations.moveFileToDirectory(fileZip.getAbsolutePath(), destFile.getAbsolutePath(), false);
			//FileUtils.moveFileToDirectory(fileZip, destFile, false);
			log.info("Done ! Moving file zip to " + destFile.getAbsolutePath());
		} catch (IOException e) {		
			e.printStackTrace();
			log.info("Exception : " + e.toString());
			return null;
		}
		File destinationFile = new File(destinationFolder + File.separator + fileZip.getName());
		if(destinationFile.exists())
		{
			log.info("Exist file destinationFile  = " + destinationFile.getAbsolutePath());
			return destinationFile.getAbsolutePath();
		}
		
		log.info("destinationFile is not EXIST !!!!!! " + destinationFile.getAbsolutePath());		
		return null;
	}
	
	
}
