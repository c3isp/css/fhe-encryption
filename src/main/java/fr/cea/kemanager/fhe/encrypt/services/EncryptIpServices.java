package fr.cea.kemanager.fhe.encrypt.services;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.cea.kemanager.encrypt.models.BinaryArrayObject;
import fr.cea.kemanager.fhe.encrypt.helper.*;
import fr.cea.kemanager.fhe.encrypt.repository.FheBlacklistAnalysisRespository;
import fr.cea.kemanager.fhe.encrypt.vault.io.BashTools;
import fr.cea.kemanager.fhe.encrypt.vault.io.FileOperations;

public class EncryptIpServices implements Runnable  {

	private static final Logger log = LoggerFactory.getLogger(EncryptIpServices.class);
	private final String m_urlBaseParameter = "/fhe/invoke-data-analysis/RequestId/{RequestID}/dsa/{DsaID}/scheme/{FHEModel}";
	private final String m_urlSaveEncryptedIp = "/fhe/fhe-dpo-id/RequestId/{RequestID}/dsa/{DsaID}/scheme/{FHEModel}";
			
	private Thread 				m_thread;
	private String 				m_rootDir;
	private String 				m_baseDir;
	private String 				m_threadName = "[FHE - Encryption]: ";
	private String 				m_dsaId; 
	private List<String> 		m_dataIPAnalysis;
	private String 				m_requestID;
	private String 				m_eventNotifyUrl;
	private String 				m_invokeFheAnalysisUrl;
	private String 				m_FHEModel;
	private boolean 			m_isNeedAnalysis;	
	private FheBlacklistAnalysisRespository 
								m_fheBlacklistAnalyisiController;
		
	public EncryptIpServices() {
	}
		
	public EncryptIpServices(String rootDir, String baseDir, String dsaId, String requestID) {		
		this.m_baseDir 				= baseDir;
		this.m_rootDir 				= rootDir;
		this.m_dsaId 				= dsaId; 
		this.m_requestID 			= requestID;
	}
	
	public EncryptIpServices(
			String rootDir, 
			String baseDir,
			String dsaId, 
			String requestID,
			String fheModel, 
			String notifyUrlToBack, 
			List<String> 		dataIPAnalysis, 
			boolean isNeedAnalysis, 
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController) {
		this(rootDir, baseDir, dsaId, requestID);
		this.m_eventNotifyUrl = notifyUrlToBack;
		this.m_dataIPAnalysis = dataIPAnalysis;
		this.m_isNeedAnalysis = isNeedAnalysis;
		this.m_FHEModel = fheModel; 
		if(fheBlacklistAnalyisiController != null)
		{
			this.m_fheBlacklistAnalyisiController = fheBlacklistAnalyisiController;
			this.m_fheBlacklistAnalyisiController.init(requestID, dsaId, fheModel);
		}
	}

	@Override
	public void run() {
		if (m_thread == null)	m_thread = new Thread (this);		
		String dataWorkingIpFolder = m_baseDir; 
		String fileLock = dataWorkingIpFolder + File.separator + Constants.LOCK_FILE;
		try {
			/*
			 * Synchronization using semaphore token : try to avoid overwriting
			 * */
			while(FileOperations.IsExisted(fileLock)) Thread.sleep(100);			
			FileOperations.PrintData(fileLock, "1".getBytes());				
			for(int i = 0; i < m_dataIPAnalysis.size(); i++)
			{
				String ipTatget = m_dataIPAnalysis.get(i);
				String ipFolder = String.format(Constants.FheAlgo.BLACKLIST.IP_FOLDER_FORMAT, i); 
				String dataIpStorageFolderTarget = String.format(
							Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, this.m_dsaId + File.separator 
							+ this.m_requestID, this.m_FHEModel) + ipFolder; 				
				try {
					ToolsFolder.CreateAllFolderNames(dataWorkingIpFolder);
				} catch (SecurityException | IOException e) {
					log.error(m_threadName + "Error during create folders for " + dataWorkingIpFolder);
					return;
				}
				
				log.info(m_threadName + "Encrypting FHE analysis over IP : " + ipTatget);			
				log.info(m_threadName + String.format("%s %s %s %s %s", 
						dataWorkingIpFolder + Constants.FheScripts.FHE_ECNRYPT_IP_NAME,
						this.m_FHEModel,
						ipTatget,
						Constants.PARTIAL_DATA_FILE,
						dataIpStorageFolderTarget));
				
				BashTools.Exec(true,
						dataWorkingIpFolder + Constants.FheScripts.FHE_ECNRYPT_IP_NAME, 
						this.m_FHEModel,
						ipTatget, 
						Constants.PARTIAL_DATA_FILE,
						dataIpStorageFolderTarget);
				/*
				 * Write dsaData.txt in the dataIpStorageFolderTarget
				 * */
				String dsaDATA = dataIpStorageFolderTarget + File.separator + Constants.DSA_FILE;
				FileOperations.PrintData(dsaDATA, this.m_dsaId.getBytes());
				
				if(m_isNeedAnalysis)
				{
					__SendingAndInvokeBlackListAnalysis(); 
				}
			}
			FileOperations.Shred(fileLock);
			/*
			 * Synchronization leave token
			 * */
		} catch (InterruptedException  | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				FileOperations.Shred(fileLock);
				m_thread.interrupt();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		log.info(m_threadName + "End encrypting IP in FHE format");		
	}

	private void __SendingAndInvokeBlackListAnalysis() throws IOException {

		log.info(m_threadName + "Send encrypted IP in FHE format to IAI analysis with urlToBack = " + this.m_eventNotifyUrl);
		Tools.InvokeIAIBlacklistAnalysis(
				m_fheBlacklistAnalyisiController, 
				m_dsaId, 
				m_requestID, 
				this.m_eventNotifyUrl, 
				this.m_FHEModel);
		
//		RestTemplate restTemplate = new RestTemplate(); 		
//
//		String urlParameter = m_urlBaseParameter
//							.replaceAll("\\{" + "RequestID" + "\\}", this.m_requestID)
//							.replaceAll("\\{" + "DsaID" + "\\}", this.m_dsaId)
//							.replaceAll("\\{" + "FHEModel" + "\\}", Constants.FheAlgo.BLACKLIST);		
//
//		
//		String fullUrlInvokeFheAnalysis = this.m_invokeFheAnalysisUrl + urlParameter;
//		
//		//org.springframework.web.client.HttpClientErrorException: 415 Unsupported Media Type
//		ResponseEntity<String>response = restTemplate.postForEntity(fullUrlInvokeFheAnalysis, urlToBack, String.class);
//		log.info(m_threadName + "Server response when invoking IAI analysis : code " + response.getStatusCodeValue() + " & " + response.getBody());		
	}


	/**
	 * ctiFile contains some information like this : dst=1:54.213.116.149
	 * @param fieldToEncrypt
	 * @param ctiFile
	 * @return
	 */
	public List<String> __getIpFromEncryptedPattern(
			@NotNull String fieldToEncrypt, 
			@NotNull String ctiFile)
	{		
		String[] actualContent = ctiFile.split("\\|");
		List<String> _lstStrIp = new ArrayList<String>();
		for (String s : actualContent) {			
			String[] parts = s.split(" ");
			for (int i=0; i<parts.length;i++) {
				if(parts[i].contains(fieldToEncrypt+"=") == false)
					continue;
								
				
				String motifEnryptedIp = parts[i].replaceAll(fieldToEncrypt+"=", ""); 
				if(motifEnryptedIp.contains(":")) {
					_lstStrIp.add(motifEnryptedIp.split(":")[1]);					
				}
			}
		}
		return _lstStrIp;
	}
}
