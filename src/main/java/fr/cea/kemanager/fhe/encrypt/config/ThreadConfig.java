package fr.cea.kemanager.fhe.encrypt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.core.task.TaskExecutor;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


/**

 * Created by Thanh-hai nguyen on 01/10/19.

 */

@Configuration
public class ThreadConfig {

    @Bean
    public TaskExecutor threadPoolTaskExecutor(@Value("${server.application.maxThreads}") int maxThreads) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(maxThreads);        
        executor.setMaxPoolSize(maxThreads);
        executor.setThreadNamePrefix("Mulithread_Task_#");
        executor.initialize();
        return executor;

    }

}
