package fr.cea.kemanager.fhe.encrypt.restapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.http.NoHttpResponseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cea.kemanager.encrypt.models.EncryptObjectRequest;
import fr.cea.kemanager.fhe.encrypt.components.BufferManagerComponent;
import fr.cea.kemanager.fhe.encrypt.helper.Constants;
import fr.cea.kemanager.fhe.encrypt.helper.ToolControllers;
import fr.cea.kemanager.fhe.encrypt.helper.Tools;
import fr.cea.kemanager.fhe.encrypt.helper.ToolsFolder;
import fr.cea.kemanager.fhe.encrypt.models.DposArrObj;
import fr.cea.kemanager.fhe.encrypt.models.KeyObject;
import fr.cea.kemanager.fhe.encrypt.models.KeyTransfer;
import fr.cea.kemanager.fhe.encrypt.models.ResultEncryptedData;
import fr.cea.kemanager.fhe.encrypt.repository.FheBlacklistAnalysisRespository;
import fr.cea.kemanager.fhe.encrypt.repository.FheKeyController;
import fr.cea.kemanager.fhe.encrypt.vault.io.BashTools;
import fr.cea.kemanager.fhe.encrypt.vault.io.FileOperations;
import fr.cea.kemanager.lib.DataObject;
import io.swagger.annotations.ApiParam;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-03-16T10:37:10.348Z")

@RestController
@RequestMapping("/v1")
public class FheCingulataApiImpl implements FheCingulataApi {
	
    @Autowired
    private TaskExecutor taskExecutor;

	private final String logStatus = "[FHE-CINGULATA-MANAGER] ";
    private final ObjectMapper objectMapper;
    private final HttpServletRequest request;    
    
    private static HashMap<String, String> lstLimiteRequest = new HashMap<String, String>();

    @Value("${token.c3isp.cnr.it}")
    String TokenRoot;    
    
    @Value("${rest.endpoint.url.callFheKeyCoreBase}")
	private String callFheKeyBaseEndpoint;
    
	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
			
	//@Value("${cea.fhe.algorithmes}")
	//private String FHE_ALGORITHM;
	
	@Autowired
	private FheKeyController fheKeyController;
	
	@Autowired
	private FheBlacklistAnalysisRespository fheBlacklistAnalyisiController;
	
	@Autowired
	private BufferManagerComponent bufferManagerComponent;
	
	@Autowired
    public FheCingulataApiImpl(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }
    
    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<DataObject> decrypt(    		
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel, 
    		@ApiParam(value = "Ciphertext file") @RequestPart(value="cipherTextFile") MultipartFile cipherTextFile) {
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {                	                
                	log.info(logStatus + "ResponseEntity<ServerResponse> encrypt method start working");
                	/**
                	 * Checking existing key ?
                	 * */                	
                	String fileName = cipherTextFile.getName(); 
					byte[] content = cipherTextFile.getBytes();
					File ctFile = Tools.createTempFile("m_0", ".ct", content);
					log.info(logStatus + " the temp file was created at " + ctFile.getAbsolutePath() + " at parent = " + ctFile.getParent());
					DataObject reServerResponse = decryptCipherText(requestId, dsAId, fhEModel, fileName, ctFile); 
					return new ResponseEntity<DataObject>(reServerResponse, HttpStatus.OK);           	
                } catch (NumberFormatException e) {
                    log.error("NumberFormatException Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }  catch (IOException  e) {
                  log.error("IOException Couldn't serialize response for content type application/json", e);
                  return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

	private DataObject decryptCipherText(
			String requestId, String dsAId, String fhEModel, String fileName, File ctFile)
			throws IOException {
		fheKeyController.init(requestId, dsAId, fhEModel);                	
		boolean isCreated = fheKeyController.CreatingFHEKeyIfNeed();
		if(isCreated)
		{
			/**
			 * Existed keys, process to analyse now
			 * */
			byte[] secretKey = fheKeyController.GetFheSecretKey();
			assert(secretKey != null);
			String baseDir = String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, 
					dsAId + File.separator + requestId, fhEModel);
			
			ToolsFolder.CreateAllFolderNames(baseDir);
			
			log.info(logStatus + "configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC + " " + baseDir );
			BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, baseDir);
			
			FileOperations.PrintData(baseDir + File.separator + Constants.FheKeys.FHE_SK_FILE, secretKey);
			
			
			log.info(logStatus + String.format("Exec %s %s with FolderOffileName = %s", 
					baseDir + File.separator + Constants.FheScripts.FHE_DECRYPT_IP_NAME, 
					ctFile.getParent(), 
					fileName));
			
			String result = BashTools.ExecDecryptFhe(
					baseDir + File.separator + Constants.FheScripts.FHE_DECRYPT_IP_NAME, 
					ctFile.getParent());    		
			
			DataObject reServerResponse = new DataObject()
					.dataContent(result)
					.dataName("decrypted_"+ dsAId + "_" + fhEModel)
					.checksum("checsum")
					.urlToBack("url_back");
			
			return reServerResponse;
		}
		return new DataObject();
	}

    @Override 
    public ResponseEntity<String> encrypt(
    		@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true) @PathVariable("NeedAnalysis") boolean isNeedAnalysis,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
            	String storageEncryptedIpPath = "";            
            	if(lstLimiteRequest.containsKey(requestId) == false)
            		lstLimiteRequest.put(requestId, dsAId);
            	else {            		
            		return new ResponseEntity<String>("{\"response\": \"Your request was already submited, please change request ID for invoking again !\"}", 
            				HttpStatus.OK);               
            	}
            	                
            	log.info(logStatus + "ResponseEntity<ServerResponse> encrypt method start working");
            	String contentFile = fileData.getDataContent(); 
            	String fieldToEncrypt = "";
        		if(fileData.getOtherProperties() != null && fileData.getOtherProperties().size() > 0)
        		{			
        			fieldToEncrypt = fileData.getOtherProperties().get(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM);
        			log.info(logStatus + "Received FheEncryptingField to FHE request =" 
        					+ requestId + " DSAID = " + dsAId + " field To Transcrypting = " + fieldToEncrypt
        					+ " with length object = " + fileData.getDataContent().length());
        		}
        		assert(fieldToEncrypt.isEmpty() == false);        		
            	List<String> ipToCheck = Tools.ExtractValueFromFieldText(contentFile, fieldToEncrypt);
            	/**
            	 * Checking existing key ?
            	 * */
            	try {
					storageEncryptedIpPath = ToolControllers.encryptedIPs(
							fheKeyController, 
							fheBlacklistAnalyisiController,
							fileData.getUrlToBack(), dsAId, requestId,  
							fhEModel, ipToCheck, isNeedAnalysis, /*isUsedForMultihtreading*/ false);
					
				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<String>(
	            			"{\"response\": \"Error from Save or Get Keys\"}", HttpStatus.FORBIDDEN);
				}                	            		       	   
        		log.info(logStatus + "Finished for preparation before sending the content folder : " + storageEncryptedIpPath);
        		if(storageEncryptedIpPath.length() > 0)
        			return new ResponseEntity<String>(storageEncryptedIpPath, HttpStatus.OK);
        		else 
        			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    @Override
    public ResponseEntity<DposArrObj> encryptCefFile(
    		@ApiParam(value = "The CEF content to upload", required=true) @RequestPart(value="cefFile") MultipartFile cefFile,
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true) @PathVariable("extractField") String extractField,    		
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {           
                try {
                	String msg = logStatus + "dsAId = " + dsAId + " Extract Field = " + extractField + " FHE Model = " 
							+ fhEModel + " CEF file name = " + cefFile.getName() + " with lenght " + cefFile.getBytes().length;            	
                	log.info(msg);

                	if(cefFile.getBytes() == null || cefFile.getBytes().length == 0)
                	{
    	            	ArrayList<String> dposIds = new ArrayList<String>();
    	            	dposIds.add("/opt/isi/datalakebuffer/fhe-test-dsa-00-01-02-03/ip00/ip_00.zip"); 
    	            	dposIds.add("/opt/isi/datalakebuffer/fhe-test-dsa-00-01-02-03/ip01/ip_01.zip");
    	            	dposIds.add("/opt/isi/datalakebuffer/fhe-test-dsa-00-01-02-03/ip02/ip_02.zip");
    	            	DposArrObj dao = new DposArrObj()
    	            			.success(true)
    	            			.new_elements(dposIds);
    	            	return new ResponseEntity<DposArrObj>(dao, HttpStatus.OK);
                	}
                	
                	DataObject fileData = new DataObject(); 
                	String contentFile = new String(cefFile.getBytes(), StandardCharsets.UTF_8); 
                	log.info(logStatus + "Contentfile Length = \n" + contentFile.length());
                	fileData.dataContent(contentFile)
                			.checksum("checksum")
                			.dataName(requestId+"_"+cefFile.getName());
                	Map<String, String> hsMap = new HashMap<String, String>(); 
                	hsMap.put(Constants.DefaultParam.ENCRYPTED_FIELD_PARAM, extractField); 
                	fileData.setOtherProperties(hsMap);
                	
                	ResponseEntity<String> pathToEncryptedIPs = encrypt(fileData, requestId, dsAId, false, fhEModel);
                	log.info(logStatus + " Encrypt ok, start to zip and move to VDL");
                	if(pathToEncryptedIPs.getStatusCode() == HttpStatus.OK)
                	{
                		String parentFolderEncryptedIps = pathToEncryptedIPs.getBody();
                		List<String> folderIPs = ToolsFolder.getAllChildFolderInFolderParent(parentFolderEncryptedIps);
                		ArrayList<String> arrZipIps = new ArrayList<>();
                		
                		String pathToVDL = bufferManagerComponent.createVDL();
                		log.info(logStatus + "VDLFile :" + pathToVDL);
                		log.info(String.format("%s Looking for parent Folder %s with %d child folders", 
                				logStatus, parentFolderEncryptedIps, folderIPs.size()));
                		
            			if(pathToVDL == null || pathToVDL.length() == 0)
            			{
            				throw new ExceptionInInitializerError("Cannot create VDL exception ! ");
            			}
            			
            			Path vdlDestinationPath = Paths.get(Tools.formatVDLPath(pathToVDL));
                		
                		for (String folderIP : folderIPs) {
                			
							String pathToipZipFile = ToolsFolder.zipAllFilesInFolder(folderIP, requestId, vdlDestinationPath);
							log.info(logStatus + "zip files in folder " + folderIP + " = " + pathToipZipFile);
							arrZipIps.add(pathToipZipFile);
						}
                		DposArrObj objDposArrObj = new DposArrObj().success(true).new_elements(arrZipIps);
                		return new ResponseEntity<DposArrObj>(objDposArrObj, HttpStatus.OK);
                	}
                	else 
                		return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
                	
                } catch (Exception e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    @Override
    public ResponseEntity<DposArrObj> encryptLinkCefFile(
    		@ApiParam(value = "" ,required=true) @RequestHeader(value="DSAId", required=true) String dsAId,
    		@ApiParam(value = "The extractField src or dest" ,required=true) @RequestHeader(value="param", required=true) String param,
    		@ApiParam(value = "The FHE Analysis Model" ,required=true, allowableValues = FHE_ALGORITHM) @RequestHeader(value="option", required=true) String option,
    		@ApiParam(value = "link to CEF file" ) @RequestHeader(value="bufferLinkFile", required=false) String bufferLinkFile) {
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {           
            try {
            	
            	String msg = logStatus + "dsAId = " + dsAId + " Extract Field = " + param + " FHE Model = " 
            											+ option + " bufferLink to CEF file = " + bufferLinkFile;            	
            	log.info(msg);
            	String requestId = UUID.randomUUID().toString();
            	File cefFile = new File(bufferLinkFile);
            	if(cefFile.exists()) {

            		String contentType = "text/plain";
            		byte[] content = null;
            		try {
            		    content = Files.readAllBytes(Paths.get(cefFile.getAbsolutePath()));
            		} catch (final IOException e) {
            		}
            		MultipartFile mpCefFile = new MockMultipartFile(cefFile.getName(), cefFile.getName(), contentType, content); 
            		return encryptCefFile(mpCefFile, requestId, dsAId, param, option);
            	}
            	else 
            		return new ResponseEntity<>(new DposArrObj().success(false), HttpStatus.FAILED_DEPENDENCY);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);                
                return new ResponseEntity<>(new DposArrObj().success(false), HttpStatus.INTERNAL_SERVER_ERROR);
            }           
	    } else {
	        log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
	    }
	    return new ResponseEntity<>(new DposArrObj().success(false), HttpStatus.NOT_IMPLEMENTED);
	}


    @Override
    public ResponseEntity<List<String>> getOutIpFromCEF(
    		@ApiParam(value = "The CEF content to upload", required=true) @RequestPart(value="cefFile") MultipartFile cefFile,
    		@ApiParam(value = "",required=true) @PathVariable("extractField") String extractField) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        	try {
            	String contentFile = new String (cefFile.getBytes(), StandardCharsets.UTF_8);
            	List<String> ipToCheck = Tools.ExtractValueFromFieldText(contentFile, extractField);
            	return new ResponseEntity<List<String>>(ipToCheck, HttpStatus.OK);
            } catch (Exception e) {
            	e.printStackTrace();
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
    
    @Override
    public ResponseEntity<ResultEncryptedData> encryptIp(
    		@ApiParam(value = "",required=true) @PathVariable("ip") String ip,
    		@ApiParam(value = "",required=true) @PathVariable("dsaId") String dsaId,     		
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        	try {

            	List<String> lstIps = new ArrayList<>();
            	lstIps.add(ip);            	
            	String requestID = UUID.randomUUID().toString();
            	log.info("Receiving message for encrypting IP " + ip + " from requestID " + requestID + " of DSAId "+ dsaId);
            	String targetParentFolder = ToolControllers.encryptedIPs(
            			fheKeyController, 
            			fheBlacklistAnalyisiController,
            			"", dsaId, requestID, fhEModel,   
        				lstIps, false, false);          
            	/*Get encrypted files now */
            	String ipFolder = String.format(Constants.FheAlgo.BLACKLIST.IP_FOLDER_FORMAT, 0); /*Always index 0 for encrypting IP by IP*/
            	String storageEncryptedIPPath = targetParentFolder + ipFolder;
				//String storageEncryptedIPPath = String.format(
				//			Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsaId + File.separator + requestID) + ipFolder;            	
            	ResultEncryptedData resEncryptedIp = ToolsFolder.getAllContentFilesInFolder(storageEncryptedIPPath); 
            	resEncryptedIp.setRequestID(requestID);
            	return new ResponseEntity<ResultEncryptedData>(resEncryptedIp, HttpStatus.OK);
            } catch (Exception e) {
            	e.printStackTrace();
                log.error("Couldn't serialize response for content type application/json OR cannot save or get FHE Keys", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    		        
    }
    
    @Override
    public ResponseEntity<ResultEncryptedData> encryptIpLvlSecurity(
    		@ApiParam(value = "",required=true) @PathVariable("ip") String ip,
    		@ApiParam(value = "",required=true) @PathVariable("dsaId") String dsaId,     		
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel) {
    	if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
        	try {
            	List<String> lstIps = new ArrayList<>();
            	lstIps.add(ip);            	
            	String requestID = UUID.randomUUID().toString();
            	log.info("Receiving message for encrypting IP " + ip + " from requestID " + requestID + " of DSAId "+ dsaId);
            	log.info("ToolControllers.encryptedIPs(fheKeyController, fheBlacklistAnalyisiController, "
            			+dsaId + " " + requestID + " " + fhEModel+ "lstIps, false, false");          
            	/*Get encrypted files now */
            	String ipFolder = String.format(Constants.FheAlgo.BLACKLIST.IP_FOLDER_FORMAT, 0); /*Always index 0 for encrypting IP by IP*/ 
				String storageEncryptedIPPath = String.format(
							Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsaId + File.separator + requestID, fhEModel) + ipFolder;            	
				log.info("storageEncryptedIPPath = " + storageEncryptedIPPath);
				
				/*ResultEncryptedData resEncryptedIp = ToolsFolder.getAllContentFilesInFolder(storageEncryptedIPPath); 
            	resEncryptedIp.setRequestID(requestID);
            	return new ResponseEntity<ResultEncryptedData>(resEncryptedIp, HttpStatus.OK);*/
				
            } catch (Exception e) {
            	e.printStackTrace();
                log.error("Couldn't serialize response for content type application/json OR cannot save or get FHE Keys", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    		        
    }
    
    @Override
	public ResponseEntity<DataObject> transEncryption(
			@ApiParam(value = "The CTI content to upload." ,required=true )  @Valid @RequestBody DataObject fileData,
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,			
			@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId, 
			@ApiParam(value = "The FHE Model" ,required=true, allowableValues = FHE_ALGORITHM )  
						@Valid @PathVariable("FHEModel") String FHEModel) {		
		//try {
			if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			    if (getAcceptHeader().get().contains("application/json")) {
			    	
			    	log.info("[FHE-ENCRYPTION] Received Transciphering to FHE request =" 
							+ requestId + " DSAID = " + dsAId + " with length object = " + fileData.getDataContent().length());			    	
			    	//HEKeyManager keyMng = new HEKeyManager(TokenRoot, vaultConfigTemplate.vaultTemplate());			    	
					String rootDir = Constants.FHE_REPOSITORY_DIR; 
					String baseDir = String.format(Constants.FheAlgo.BLACKLIST.PREPAREDATA_DIR, dsAId, FHEModel);
					DataObject reServerResponse = null;
//					DataObject reServerResponse = ToolControllers.FheEncryptingField(
//            			log, keyMng, rootDir, baseDir, 
//            			fileData, requestId, dsAId, FHEModel);
			    	
			    	return new ResponseEntity<DataObject>(reServerResponse, HttpStatus.OK);                   	
			    }
			}else {
			    log.warn("ObjectMapper or HttpServletRequest not configured in default DataApi interface so no example is generated");
			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
	
	@Override
	public ResponseEntity<String> invokeRemoteStorageFheData(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel)
	{
		  if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			  log.info("ResponseEntity<String> invokeStorageFheData method start working");
			  log.info("ResponseEntity<String> invokeStorageFheData method NOT IMPLEMENTATION");
			  return new ResponseEntity<String>("", HttpStatus.OK);
		  }
          return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);	
	}
	
	@Override
	public ResponseEntity<String> invokeLocalDataToFheAnalysis(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		//@ApiParam(value = "The URL to get back analysis result DPO-ID" ,required=true )  @Valid @RequestBody String urlBack,
    		@ApiParam(value = "The URL to get back analysis result DPO-ID" ,required=true )  @Valid @RequestParam("urlBack") String urlBack,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel)
	{
		if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
			if (getAcceptHeader().get().contains("application/json")) {
				
					log.info("ResponseEntity<String> invokeLocalDataToFheAnalysis method start");
					
						fheBlacklistAnalyisiController.init(requestId, dsAId, fhEModel);
						
				        taskExecutor.execute(new Runnable() {
				            @Override
				            public void run() {
				            	Tools.InvokeIAIBlacklistAnalysis(
									fheBlacklistAnalyisiController,
									dsAId, requestId, urlBack, fhEModel);
				            }
				        });						
					
					log.info("ResponseEntity<String> InvokeIAIBlacklistAnalysis start to working");
					return new ResponseEntity<String>("{\"Response\": \"IN PROCESSING\"}", HttpStatus.OK);				
			}
		}
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);	
	}

	@Override
	public ResponseEntity<String> storeDataObjectIDFromFheAnalysis(
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel,
    		@ApiParam(value = "",required=true) @Valid @RequestBody String dpoID)
    		//@ApiParam(value = "",required=true) @Valid @RequestParam("dpoID") String dpoID) 
	{
		String fileName = Constants.DPOID_FILE;
		byte[] content = Base64.getDecoder().decode(dpoID.getBytes());	
		return storeDataObjectFromFheAnalysis(requestId, dsAId, fhEModel, content, fileName);		
	}
	
	@Override public 
	ResponseEntity<String> storeDataContentObjectIDFromFheAnalysis(    		    		
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId,
    		@ApiParam(value = "",required=true, allowableValues = FHE_ALGORITHM) @PathVariable("FHEModel") String fhEModel, 
    		@ApiParam(value = "",required=true) @Valid @RequestBody EncryptObjectRequest encryptedContentFile)
	{
		String fileName = Constants.DPO_CONTENT_FILE;
		byte[] content = Base64.getDecoder().decode(encryptedContentFile.getBinaryObj().getBytes());	
		return storeDataObjectFromFheAnalysis(requestId, dsAId, fhEModel, content, fileName);
	}
		
	public ResponseEntity<String> storeDataObjectFromFheAnalysis(
			String requestId,
    		String dsAId,
    		String fhEModel, 
    		byte[] content, 
    		String fileNameToStore) 
	{
		try {
			log.info(logStatus+ "Invoke method to store object in encrypted FHE. "
	    			+ "This data may contain the FHE Blacklist analysis result from IAI/FHE platform : requestID =" 
					+ requestId + " DSAID = " + dsAId + " with model = " + fhEModel + " content size = " + content.length);			    				    	
			String databaseDir =  String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsAId + File.separator + requestId, fhEModel);
			try {
				ToolsFolder.CreateAllFolderNames(databaseDir);
				String filePathName = databaseDir + fileNameToStore;
				FileOperations.PrintData(filePathName, content);
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
	    	return new ResponseEntity<String>("{\"response\": \"SAVE\"}", HttpStatus.OK);     
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}

	@Override
	public ResponseEntity<String> checkingResult(    		
			@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId, 
    		@ApiParam(value = "The FHE Model" ,required=true )  @Valid @PathVariable("FHEModel") String FHEModel) {
    		ResponseEntity<String> ok = new ResponseEntity<String>("{\"Value\": \"ok\"}", HttpStatus.OK);
    		ResponseEntity<String> ko = new ResponseEntity<String>("{\"Value\": \"\"}", HttpStatus.OK);

			String folderResult = 
					String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsAId + File.separator + requestId, FHEModel);
			File fileData = new File(folderResult + File.separator + Constants.DPO_CONTENT_FILE);  
			if(fileData.exists())
				return ok;
			return ko;
	}

	@Override
	public ResponseEntity<String> getResult(
    		@ApiParam(value = "",required=true) @PathVariable("RequestId") String requestId,
    		@ApiParam(value = "",required=true) @PathVariable("DSAId") String dsAId, 
    		@ApiParam(value = "The FHE Model" ,required=true )  @Valid @PathVariable("FHEModel") String FHEModel) {
		String folderResult = 
				String.format(Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsAId + File.separator + requestId, FHEModel);
		String fileName = folderResult + File.separator + Constants.DPO_CONTENT_FILE;
		File fileData = new File(fileName);  		

		if(fileData.exists())
		{
			try {
				File ctFile = Tools.createTempFile("m_0", ".ct", FileOperations.GetData(fileName));
				DataObject reServerResponse = decryptCipherText(requestId, dsAId, FHEModel, ctFile.getName(), ctFile);
				return new ResponseEntity<String>("{\"Value\": \""+reServerResponse.getDataContent()+"\"}", HttpStatus.OK);
				
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
		return new ResponseEntity<String>("{\"Value\": \"Error\"}", HttpStatus.OK);
	}
}
