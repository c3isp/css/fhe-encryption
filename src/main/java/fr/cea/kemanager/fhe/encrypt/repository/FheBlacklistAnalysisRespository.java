package fr.cea.kemanager.fhe.encrypt.repository;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.cea.kemanager.encrypt.models.BinaryArrayObject;
import fr.cea.kemanager.fhe.encrypt.helper.Constants;
import fr.cea.kemanager.lib.DataObject;

@Component
public class FheBlacklistAnalysisRespository {

	Logger log = LoggerFactory.getLogger(FheBlacklistAnalysisRespository.class); 
	private final String logStatus = "[FHE-INVOKE-ANALYSIS] ";	

	private String m_baseUrl; 
	private String requestId;
	private String fheModel; 
	private String dsAId;
	
	private final int ANALYSIS_TIME_OUT = 300; //in second
	/**
	 * Used to call REST endpoints; configured by getClientHttpRequestFactory
	 */
	private RestTemplate restTemplate;	
	private String m_blAnalysisUrl 	= "/analyse/{ModelAnalysis}/{DsaID}";	
	
	@Autowired
	public FheBlacklistAnalysisRespository(
			@Value("${rest.endpoint.url.callFheBLAnalysisBase}") 	String baseUrl, 
			@Value("${security.user.name}") 						String restUser,
			@Value("${security.user.password}") 					String restPassword) {
		super();
		this.m_baseUrl = baseUrl;		
		restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.getInterceptors().add(
				  new BasicAuthorizationInterceptor(restUser, restPassword));	
	}
	
	private ClientHttpRequestFactory getClientHttpRequestFactory() {
	    
	    HttpComponentsClientHttpRequestFactory clientHttpRequestFactory
	      = new HttpComponentsClientHttpRequestFactory();
	    clientHttpRequestFactory.setConnectTimeout(ANALYSIS_TIME_OUT*1000);
	    clientHttpRequestFactory.setReadTimeout(ANALYSIS_TIME_OUT*1000);
	    return clientHttpRequestFactory;
	}
	
	
	public void init (String requestId, String dsAId, String modelAnalysis) 
	{
		this.requestId 	= requestId;
		this.dsAId 		= dsAId;	
		this.fheModel 	= modelAnalysis;
	}

	public ResponseEntity<String> invokeBlacklistAnalysis(DataObject bAObj) throws IOException
	{		
		
		String fullAnalysisURL 	= m_baseUrl + m_blAnalysisUrl.replaceAll("\\{" + "DsaID" + "\\}", dsAId)
															.replaceAll("\\{" + "ModelAnalysis" + "\\}", fheModel);			
		log.info(logStatus + "Invoking invokeBlacklistAnalysis now : " + fullAnalysisURL);
		try {
			ResponseEntity<String> resCreateKeys = restTemplate.postForEntity(fullAnalysisURL, bAObj, String.class);
			if(resCreateKeys.getStatusCode() != HttpStatus.PROCESSING)
			{
				throw new IOException(logStatus + "Impossible to invoke FHE analysis");
			}
			log.info(logStatus + "DPO-ID for <dpo-id>" + resCreateKeys.getBody() + "</dpo-id>");
			bAObj.getOtherProperties().clear();
			return resCreateKeys;
		} catch (org.apache.http.NoHttpResponseException e) {
    		log.info(logStatus + "Timeout waiting  the response "); 
		}	
		return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
	}
	
	public String getFullUrl()
	{
		return this.m_baseUrl + this.m_blAnalysisUrl;
	}
}
