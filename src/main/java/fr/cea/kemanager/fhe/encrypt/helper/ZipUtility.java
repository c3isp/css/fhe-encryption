package fr.cea.kemanager.fhe.encrypt.helper;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 * @author www.codejava.net
 *
 */

public class ZipUtility {
	/**
     * Size of the buffer to read/write data
     */
	private static Logger log = LoggerFactory.getLogger(ZipUtility.class);

    //private static final int BUFFER_SIZE = 4096;
	private static final int BUFFER_SIZE = 128;
    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
        	if(entry.getName().length() == 0) {
        		log.info("ERROR Extracting file has noname !");
        		entry = zipIn.getNextEntry();
        		continue;
        	}
            String filePath = destDirectory + File.separator + entry.getName();
            log.info("Starting Extract file " + filePath);
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            System.out.println("End Extract file " + filePath);
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        //byte[] bytesIn = new byte[4096];        
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }    

	
	public static byte[] zipContent(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = zipFile(srcFiles, fileName);
		Path path = Paths.get(file.getAbsolutePath());
		byte[] data = null;
		data = Files.readAllBytes(path);
		return data;
	}
	
	public static File zipFile(List<File> srcFiles, String fileName) throws FileNotFoundException, IOException {
		File file = Tools.createTempFile(fileName, ".zip");
		FileOutputStream   fos = new FileOutputStream(file);
		ZipOutputStream zos = new ZipOutputStream(fos);
		byte[] buffer = new byte[128];
		for (File currentFile : srcFiles) {
			if (!currentFile.isDirectory()) {
				ZipEntry entry = new ZipEntry(currentFile.getName());
				FileInputStream fis = new FileInputStream(currentFile);
				zos.putNextEntry(entry);
				int read = 0;
				while ((read = fis.read(buffer)) != -1) {
					zos.write(buffer, 0, read);
				}
				zos.closeEntry();
				fis.close();
			}
		}
		zos.close();
		fos.close();
		return file;
	}
	
	public static String extractFileInZiptoByteArray(byte[] contentBundle) throws IOException {                        
        
		String fileTempName = UUID.randomUUID().toString();
		File payloadFile 	= Tools.createTempFile(fileTempName, ".zip",  
											Base64.getDecoder().decode(contentBundle));
		//2. Extract content zip 
		Path destinationPath = Paths.get(payloadFile.getParent(), fileTempName);
		log.info("Start to unzip file "+ payloadFile.getAbsolutePath() + " into " + destinationPath.toString());
		
		ToolsFolder.CreateAllFolderNames(destinationPath.toString()); 
        
//        FileUtils.writeStringToFile(tmpFile, Base64.decodeAsString(evts[0].getFileContent()),Charset.forName("UTF-8"));        
        
        ZipFile zipFile = new ZipFile(payloadFile);
        
        File tmpContent = Tools.createTempFile(destinationPath + File.pathSeparator + "testzipcont1", Constants.BundleManagerSuffixFile.CTI_SUFFIX);
        File tmpSignContent = Tools.createTempFile(destinationPath + File.pathSeparator + "testsincont1", Constants.BundleManagerSuffixFile.HASH_SUFFIX);
        FileUtils.forceDeleteOnExit(tmpContent);
        log.info("File zip to test " + payloadFile.getAbsolutePath() + " content payload file = " + tmpContent.getAbsolutePath());
        
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            log.info("looking for file payload in " + entry.getName());                        
 
            if (entry.getName().contains(Constants.BundleManagerSuffixFile.CTI_SUFFIX)) {
                FileUtils.copyInputStreamToFile(zipFile.getInputStream(entry), tmpContent); 
            }
            if (entry.getName().contains(Constants.BundleManagerSuffixFile.HASH_SUFFIX)) {
                FileUtils.copyInputStreamToFile(zipFile.getInputStream(entry), tmpSignContent); 
            }
        }
        return destinationPath.toString();
	}
		
}
