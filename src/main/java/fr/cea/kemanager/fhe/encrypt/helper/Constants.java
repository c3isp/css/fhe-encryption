package fr.cea.kemanager.fhe.encrypt.helper;

import java.io.File;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import springfox.documentation.spring.web.paths.Paths;

@SuppressWarnings("unused")
public class Constants {
	public enum AnalysisMethodeEnum {
	    BLACK_LIST_FULL("BLACK_LIST_FULL"),
	    
	    OTHER("OTHER"),;

	    private String value;

	    AnalysisMethodeEnum(String value) {
	      this.value = value;
	    }

	    @Override
	    @JsonValue
	    public String toString() {
	      return String.valueOf(value);
	    }

	    @JsonCreator
	    public static AnalysisMethodeEnum fromValue(String text) {
	      for (AnalysisMethodeEnum b : AnalysisMethodeEnum.values()) {
	        if (String.valueOf(b.value).equals(text)) {
	          return b;
	        }
	      }
	      return null;
	    }
	  }
	
	public static final String SEPARATOR_PARAM 	= "_TRANS_";
	/*BASE PRODUCTION*/	
	private static final String USR_BASE 				= "/opt";
	private static final String FHE_BASE 				= USR_BASE + File.separator + "FHE_CIN";
	public static final String FHE_REPOSITORY_DIR 		= USR_BASE + File.separator + "FHE_C3ISP" + File.separator + "repository";
	private static final String FHE_ALGO_DIR 			= FHE_BASE + File.separator + "ALGO"; 	
	private static final String FHE_PREPAREDATA_DIR 	= FHE_BASE + File.separator + "C3ISP" + File.separator + "prepareData";
	private static final String FHE_ANALYSISDATA_DIR 	= FHE_BASE + File.separator + "C3ISP" + File.separator + "analysisData";
	private static final String FHE_DATABASE_DIR 		= FHE_BASE + File.separator + "C3ISP" + File.separator + "localDataBase";
	public static final String INPUT_ANALYSIS_FOLDER	= "input";
	public static final String OUTPUT_ANALYSIS_FOLDER	= "output";	
	public static final String FHE_RESULT_FOLDER		= "analysis";
	public static final String LOCK_FILE 				= "lock.ini";
	public static final String DSA_FILE 				= "dsa.ini";
	public static final String PARTIAL_DATA_FILE 		= "partialData.data";
	public static final String DPOID_FILE 				= "dpo_id.data";
	public static final String DPO_CONTENT_FILE			= "dpo_content.ct";	
	
	public static class FheScripts
	{
		public static final String FHE_GENERATE_KEYS_NAME 	= "generate_for_java.sh";
		public static final String FHE_CONFIG_FOLDER_EXEC 	= "configuration.sh";
		public static final String FHE_ECNRYPT_DATA_NAME 	= "00-prepareBlackList.sh";
		public static final String FHE_ECNRYPT_IP_NAME 		= "01-ecryptIPTarget.sh";
		public static final String FHE_DECRYPT_IP_NAME		= "03-decrypt.sh";
		public static final String FHE_BLACKLIST_ANALYSIS_NAME = "02-run.sh";
		
		public static final String FHE_GENERATE_KEYS = FHE_BASE + File.separator + FHE_GENERATE_KEYS_NAME;
		public static final String FHE_CONFIG_EXEC = FHE_REPOSITORY_DIR + File.separator + FHE_CONFIG_FOLDER_EXEC;				
	}		
	
	public static class FheAlgo
	{
		public static final String BLACKLIST_CIRCUIT 	= "membership.blif";
		public static final String BLACKLIST_CIRCUIT_OPT = "membership-opt.blif";
		public static final String BLACKLIST_FULL_NAME 		= "BLACK_LIST_FULL";
		public static final String BLACKLIST_HIGH_NAME 		= "BLACK_LIST_HIGH";
		public static final String BLACKLIST_MEDIUM_NAME 	= "BLACK_LIST_MEDIUM";
		public static final String BLACKLIST_LOW_NAME 		= "BLACK_LIST_LOW";

		
		public static class BLACKLIST
		{
			public static final String DATABASE_DIR 	= FHE_DATABASE_DIR     + File.separator + "%s" + File.separator + "%s" + File.separator;
			public static final String KEYDATA_DIR 		= FHE_DATABASE_DIR     + File.separator + "%s" + File.separator + "%s" + File.separator + "KeyStorage";
			public static final String PREPAREDATA_DIR 	= FHE_PREPAREDATA_DIR + File.separator + "%s" + File.separator + "%s" + File.separator;
			public static final String ANALYSISDATA_DIR = FHE_ANALYSISDATA_DIR + File.separator + "%s" + File.separator + "%s" + File.separator;
			public static final String IP_FOLDER_FORMAT = "IP_%d";
		}
	}

	public class FheKeys
	{
		public static final String FHE_PATH_VAULT = "secret/";
		public static final String FHE_EVAL_FILE = "fhe_key.evk";
		public static final String FHE_SK_FILE 	= "fhe_key.sk";
		public static final String FHE_PK_FILE 	= "fhe_key.pk";
		public static final String FHE_PARAM_FILE = "fhe_params.xml";
	}
	
	public class FheConfigKeys
	{
		public static final String FHE_EVAL_SUFFIX 	= "evk";
		public static final String FHE_SK_SUFFIX 	= "sk";
		public static final String FHE_PK_SUFFIX 	= "pk";
		public static final String FHE_PARAM_SUFFIX = "xml";
		public static final String FHE_VERSION_SUFFIX = "ver";
		public static final String FHE_VERSION_VALUE = "value";
	}
	
	public class DefaultParam
	{
		public static final String REQUEST_ID_PARAM 	= "requestID";
		public static final String PAYLOAD_FORMAT_PARAM = "payloadFormat";		
		public static final String CONTENT_FILE_PARAM 	= "fileContent";
		public static final String ENCRYPTED_FIELD_PARAM = "encryptedField";
		public static final String OFFSET_KEYSTREAM_PARAM = "offsetTranscrypting";
	}
	
	public class EventHandleParam
	{		
		public static final String METADATA_FILE_PARAM 	= "metadataFile";
		public static final String CTI_FILE_PARAM 		= "ctiFile";
		public static final String DSA_ID_PARAM 		= "dsaId";
		public static final String DPOS_ID_PARAM 		= "dposId";		
	}
	
	public class BundleResponseParam
	{				
		public static final String FILE_NAME_PARAM 		= "fileName";
		public static final String STATUS_RESULT_PARAM 	= "result";	
	}
	
	public class C3ispComponents
	{
		public static final String ISI_NODE 			= "isic3isp";
		public static final String CSS_NODE	 			= "cssc3isp";		
		public static final String ISI_BM_COMPONENT 	= "isibmcom";
		public static final String CSS_DPOS_COMPONENT	= "cssdposcom";
	}
	
	public class BundleManagerSuffixFile
	{
		public static final String METADATA_SUFFIX 	= ".head";
		public static final String CTI_SUFFIX 		= ".payload";
		public static final String DSA_SUFFIX 		= ".dsa";
		public static final String HASH_SUFFIX 		= ".sign";
	}
	
	public class FHEModel
	{
		public static final String FHE_CONN_MALIC 	= "fhe_conn_malic";
	}
}
