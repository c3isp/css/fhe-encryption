package fr.cea.kemanager.fhe.encrypt.models;

import java.util.ArrayList;

public class DposArrObj {
	
	public DposArrObj() {
		success = false;
		new_elements = new ArrayList<>();  
	} 
	
	public DposArrObj(boolean message, ArrayList<String> dposList) {
		super();
		this.success = message;
		this.new_elements = dposList;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public DposArrObj success(boolean success) {
		this.success = success;
		return this;
	}
	
	public DposArrObj new_elements(ArrayList<String>  new_elements) {
		this.new_elements = new_elements;
		return this;
	}
	
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ArrayList<String> getNew_elements() {
		return new_elements;
	}

	public void setNew_elements(ArrayList<String> new_elements) {
		this.new_elements = new_elements;
	}

	private boolean success; 
	private ArrayList<String>  new_elements;	
	
}
