package fr.cea.kemanager.fhe.encrypt.vault;

import java.nio.charset.StandardCharsets;

public class KVEncryption {

	/**
	 * This class encapsulates the operations for Kreyvium encryption from Vault.
	 *
	 * @author Quentin Lutz
	 */

	private KVKeyManager m_keyMng;

	/**
	 * Create a new {@link KVEncryption} with a {@link KVKeyManager}.
	 *
	 * @param keyMng must not be {@literal null}.
	 */
	public KVEncryption(KVKeyManager keyMng) {
		this.m_keyMng = keyMng;
	}

	/**
	 * Encrypt data with the KV key of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param plaintext is the data to be encrypted.
	 */
	public byte[] EncryptKV(byte[] plaintext,String userID) {
		m_keyMng.CreateIfNull(userID);
		int ver = m_keyMng.GetVersion(userID);
		return (String.valueOf(ver)+":" 
				+ new String(BasicCipher(plaintext, userID, ver),
						StandardCharsets.ISO_8859_1))
				.getBytes(StandardCharsets.ISO_8859_1);
	}


	/**
	 * Decrypt data with the KV key of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param ciphertext is the data to be decrypted. 
	 */
	public byte[] DecryptKV(byte[] ciphertext,String userID) {
		String str = new String(ciphertext, StandardCharsets.ISO_8859_1);
		int piv = str.indexOf(":");
		int ver = Integer.valueOf(str.substring(0, piv));
		return BasicCipher(str.substring(piv+1)
				.getBytes(StandardCharsets.ISO_8859_1),
				userID,
				ver);
	}


	public String EncryptIPKV(String txtValueToEncrypt, String userID) {
		int ver = m_keyMng.GetVersion(userID);
		int[] key = m_keyMng.GetKVKey(userID,ver);
    	String[] ip_parts = txtValueToEncrypt.split("\\.");
    	byte[] bytes = new byte[ip_parts.length];
    	for (int i=0; i<ip_parts.length;i++) {
    		bytes[i] =(byte) (Integer.valueOf(ip_parts[i])-128);
    	}
		int mySaveOffset = 0;
		String[] results = new String[ip_parts.length];
		KreyviumCipher cipher=new KreyviumCipher(key, mySaveOffset);
		byte[] encrypted = cipher.EncryptingRequest(bytes);
		for (int i=0; i<ip_parts.length;i++) {
			results[i] = Integer.toString(((int) encrypted[i]) + 128 );
		}
		String result = results[0];
		for (int i=1; i<results.length; i++) {
			result += "." + results[i];
		}
		return String.valueOf(ver)+":" +result;
	}
	
	public String DecryptIPKV(String encryptedIP, String userID) {
		int piv = encryptedIP.indexOf(":");
		int ver = Integer.valueOf(encryptedIP.substring(0, piv));
		int[] key = m_keyMng.GetKVKey(userID,ver);
		int mySaveOffset = 0;
		String[] ip_parts = encryptedIP.substring(piv+1).split("\\.");
		byte[] bytes = new byte[ip_parts.length];
		for (int i=0; i<ip_parts.length; i++) {
			bytes[i] = (byte)  (Integer.valueOf(ip_parts[i])-128);
		}
		KreyviumCipher cipher=new KreyviumCipher(key, mySaveOffset);
		bytes = cipher.EncryptingRequest(bytes);
		String[] results = new String[ip_parts.length];
		for (int i=0; i<ip_parts.length;i++) {
			results[i] = Integer.toString(((int) bytes[i])+128);
		}
		String result = results[0];
		for (int i=1; i<results.length; i++) {
			result += "." + results[i];
		}
		return result;
	}
	
	/**
	 * Encrypt data with the KV key of a given user.
	 *
	 * @param userID is the ID for the active user.
	 * @param plaintext is the data to be encrypted.
	 */
	private byte[] BasicCipher(byte[] plaintext,String userID,int version) {
		int[] key = m_keyMng.GetKVKey(userID,version);
		int mySaveOffset = 0;
		KreyviumCipher cipher=new KreyviumCipher(key, mySaveOffset);
		plaintext = cipher.EncryptingRequest(plaintext);
		return plaintext;
	}

}
