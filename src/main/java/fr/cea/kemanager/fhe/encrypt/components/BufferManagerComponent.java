package fr.cea.kemanager.fhe.encrypt.components;

import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.cea.kemanager.fhe.encrypt.models.DataLakeStatus;

@Component("BufferManagerComponent")
public class BufferManagerComponent{
	private static final Logger log = LoggerFactory.getLogger(BufferManagerComponent.class);

	private String callprepareEmptyDataLake;
	private String typeEmptyDataLake;
	private String dataLakeCategory;		
	private RestTemplate m_restTemplate;
		
	@Autowired
	public BufferManagerComponent(
			@Value("${rest.endpoint.url.callprepareEmptyDataLake}") String callprepareEmptyDataLake, 
			@Value("${rest.endpoint.url.dataLakeCategory}") String dataLakeCategory,
			@Value("${rest.endpoint.url.typeEmptyDataLake}") String typeEmptyDataLake, 
			@Value("${security.user.name}") 				String restUser, 
			@Value("${security.user.password}") 			String restPassword) {
		super();		
		this.m_restTemplate = new RestTemplateBuilder().basicAuthorization(restUser, restPassword).build(); 
		this.callprepareEmptyDataLake 	= callprepareEmptyDataLake;
		this.dataLakeCategory 			= dataLakeCategory; 
		this.typeEmptyDataLake 			= typeEmptyDataLake;
	}
	
	
	public String createVDL() {	
		String _endPoint = callprepareEmptyDataLake.replaceAll("\\{" + "dataLake" + "\\}", dataLakeCategory)
												.replaceAll("\\{" + "type" + "\\}", typeEmptyDataLake);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(headers);
		
		log.debug("FHE-CINGULATA | sending post request for : " + _endPoint);
		ResponseEntity<DataLakeStatus> bufferCreate = m_restTemplate.postForEntity(_endPoint, entity, DataLakeStatus.class);
		log.debug("FHE-CINGULATA | code Status "+ bufferCreate.getStatusCode()); 
		if(bufferCreate.getStatusCode() != HttpStatus.CREATED)			
			return null;
		else 
		{
			log.debug("FHE-CINGULATA | URI = " + bufferCreate.getBody().getURI()); 
			return bufferCreate.getBody().getURI();
		}
	}
}
