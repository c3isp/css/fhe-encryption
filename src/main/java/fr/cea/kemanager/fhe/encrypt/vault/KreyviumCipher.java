package fr.cea.kemanager.fhe.encrypt.vault;

/**
 *
 * This class encapsulates the Kreyvium cipher.
 * 
 * @author Thanh Hai Nguyen
 */
public class KreyviumCipher {


	public KreyviumCipher(int[] my_key, int my_offset)
	{
		key = new int[128];
		iv = new int[128];            
		for (int i = 0; i < 128; i++)
		{
			key[i]  = my_key[i];
		}
		b       = 0;
		iv      = new int[288];
		s       = new int[288];
		rkey    = new int[128];
		riv     = new int[128];
		k 		= 0;
		setMySaveOffset(my_offset);
		byteOffset = 0;
		heatUp();
	}

	public byte[] EncryptingRequest(byte[] request)
	{
		for(int i=0;i<request.length;i++) {
			byte k=NextKeystreamByte();
			request[i]^=k;
		}
		return request;
	}

	/**
	 * This function is only applied for data of 16 bit
	 * */
	public int[] EncryptingRequest(int[] request)
	{
		return EncryptingRequest(request, 16);
	}

	public int[] EncryptingRequest(int[] request, int sizeBit)
	{
		for(int i=0;i<request.length;i++) {
			int k=NextKeystreamWithSizebits(sizeBit);
			request[i]^=k; // Now we'are encrypted...
		}
		return request;
	}

	/*
	 * Caution ! We take 8bit to encrypt data. 
	 * This information based on algorithm written 
	 * in FHE form to decide what is kind of Integer8 or 16 will be used
	 */
	public byte NextKeystreamByte() {
		byte b=0;
		int nbrUsedBit = mySaveOffset * 8, iBitIter = 0;
		for(int i=0;i<8;i++) {
			if(iBitIter < nbrUsedBit){
				NextKeystreamBit();
				++iBitIter;
			}
			/*Warning ! generating a kreyvium token in the order of the most signification bit first 
			 * so this formula
			 * m_StoreTokenKreyviumChipher[m_LastConsumedTokenOffset] << i  
			 * --> will provide error on the server side
			 */
			b |= (byte) (NextKeystreamBit() << (8-i-1) );
		}
		++this.byteOffset;
		return b;
	}

	public int NextKeystreamWithSizebits(int sizebit) {
		int b=0;        
		int nbrUsedBit = mySaveOffset * sizebit, iBitIter = 0;
		for(int i=0;i<sizebit;i++) {
			if(iBitIter < nbrUsedBit){
				NextKeystreamBit();
				++iBitIter;
			}
			/*Warning ! generating a kreyvium token in the order of the most signification bit first 
			 * so this formula
			 * m_StoreTokenKreyviumChipher[m_LastConsumedTokenOffset] << i  
			 * --> will provide error on the server side
			 */
			b |= (int) (NextKeystreamBit() << (sizebit-i-1) );
		}
		++this.byteOffset;
		return b;
	}

	public int NextKeystreamBit()
	{
		if(k==46) {                
			nextIV();
			heatUp();
			k=0;
		}
		k++;
		int t1=s[65]^s[92];
		int t2=s[161]^s[176];
		int t3=s[242]^s[287]^rkey[127-0];
		int z=t1^t2^t3;
		t1=t1^s[90]&s[91]^s[170]^riv[127-0];
		t2=t2^s[174]&s[175]^s[263];
		t3=t3^s[285]&s[286]^s[68];
		int t4=rkey[127-0];
		int t5=riv[127-0];
		for(int j=0;j<288-1;j++)
			s[288-j-1]=s[288-j-2];
		s[0]=t3;
		s[93]=t1;
		s[177]=t2;
		for(int j=0;j<128-1;j++)
		{
			rkey[128-j-1]=rkey[128-j-2];
			riv[128-j-1]=riv[128-j-2];
		}
		rkey[0]=t4;
		riv[0]=t5;

		return z;
	}

	// Strictly speaking, there is NO way we can practically cycle
	// through the 2^128 IV, so we purposedly do not care
	// about loop termination...
	private void nextIV() {
		int j;
		b=1-b;
		if(b==1)
			j=0;
		else
		{
			j=1;
			while(iv[j-1]!=1)
				j++;
		}
		iv[j]=1-iv[j];
	}

	// Reinit internal state and heat it up...
	private void heatUp() {

		// Internal state setup.
		for(int i=0;i<288;i++)
			s[i]=1;
		// Insert the first 93 key bits @ position 0 (1 in the paper).
		for(int i=0;i<93;i++)
			s[i]=key[i];
		// Insert the first 84 iv bits @ position 93 (94 in the paper).
		for(int i=0;i<84;i++)
			s[i+93]=iv[i];
		// Insert the remaining iv bits @ position 177 (178 in the paper).
		for(int i=0;i<44;i++)
			s[i+177]=iv[i+84];
		// Set pos 287 to 0 (288 in the paper).
		s[287]=0;

		// RKEY register set up.
		for(int i=0;i<128;i++)
			rkey[i]=key[128-i-1];
		// RIV register setup.
		for(int i=0;i<128;i++)
			riv[i]=iv[128-i-1];

		for(int i=0;i<4*288;i++) {
			int t1=s[65]^s[90]&s[91]^s[92]^s[170]^riv[127-0];
			int t2=s[161]^s[174]&s[175]^s[176]^s[263];
			int t3=s[242]^s[285]&s[286]^s[287]^s[68]^rkey[127-0];
			int t4=rkey[127-0];
			int t5=riv[127-0];
			for(int j=0;j<288-1;j++)
				s[288-j-1]=s[288-j-2];
			s[0]=t3;
			s[93]=t1;
			s[177]=t2;
			for(int j=0;j<128-1;j++)
			{
				rkey[128-j-1]=rkey[128-j-2];
				riv[128-j-1]=riv[128-j-2];
			}
			rkey[0]=t4;
			riv[0]=t5;
		}
	}

	public void SetValueIV(int index, int value)
	{
		iv[index] = value;
	}

	public int[] getKey() {
		return key;
	}

	public void setKey(int[] key) {
		this.key = key;
	}

	public int[] getIv() {
		return iv;
	}

	public void setIv(int[] iv) {
		this.iv = iv;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getByteOffset() {
		return byteOffset;
	}

	public void setByteOffset(int byteOffset) {
		this.byteOffset = byteOffset;
	}


	public int getMySaveOffset() {
		return mySaveOffset;
	}

	public void setMySaveOffset(int mySaveOffset) {
		this.mySaveOffset = mySaveOffset;
	}


	private int[] key;
	private int[] iv;
	private int[] s;

	private int[] rkey;
	private int[] riv;
	private int b; 
	private int k;
	private int byteOffset;
	private int mySaveOffset; 
}
