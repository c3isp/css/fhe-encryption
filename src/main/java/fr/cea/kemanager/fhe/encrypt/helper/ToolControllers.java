package fr.cea.kemanager.fhe.encrypt.helper;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.cea.kemanager.fhe.encrypt.ApplicationDeployer;
import fr.cea.kemanager.fhe.encrypt.models.KeyObject;
import fr.cea.kemanager.fhe.encrypt.models.KeyTransfer;
import fr.cea.kemanager.fhe.encrypt.repository.FheBlacklistAnalysisRespository;
import fr.cea.kemanager.fhe.encrypt.repository.FheKeyController;
import fr.cea.kemanager.fhe.encrypt.restapi.FheCingulataApi;
import fr.cea.kemanager.fhe.encrypt.models.KeyObject.KeyInfoEnum;
import fr.cea.kemanager.fhe.encrypt.services.*;
import fr.cea.kemanager.fhe.encrypt.vault.io.*;
import fr.cea.kemanager.lib.DataObject;

public class ToolControllers {
	
	private static Logger log = LoggerFactory.getLogger(ToolControllers.class);
	
	private static byte[] GetHEPublicKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	private static byte[] GetHEEvaluationKey(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_EVK) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	private static byte[] GetHEParameter(List<KeyObject> lstKeys)
	{
		for(KeyObject keyO : lstKeys)
		{
			if(keyO.getKeyInfo().compareTo(KeyInfoEnum.FHE_PARAM) == 0)
				return keyO.getKey();
		}
		return null;
	}
	
	public static boolean FheEncryptingField(			
			KeyTransfer lstKeys,
			String rootDir,
			String baseDir,
			List<String> ipToCheck, 
			String requestId, 
			String dsAId, 
			String FHEModel, 			
			String urlToBack,
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController) throws IOException {
		return FheEncryptingField(			
				lstKeys,
				rootDir,
				baseDir,
				ipToCheck, 
				requestId, 
				dsAId, 
				FHEModel, urlToBack, true, true,
				fheBlacklistAnalyisiController);
	}
	
	public static boolean FheEncryptingField(			
			KeyTransfer lstKeys,
			String rootDir,
			String baseDir,
			List<String> ipToCheck,
			String requestId, 
			String dsAId, 
			String FHEModel, 
			String urlToBack) throws IOException {
		return FheEncryptingField(			
				lstKeys,
				rootDir,
				baseDir,
				ipToCheck, 
				requestId, 
				dsAId, 
				FHEModel, urlToBack, false, true, null);
	}
	
	public static boolean FheEncryptingFieldNoThread(			
			KeyTransfer lstKeys,
			String rootDir,
			String baseDir,
			List<String> ipToCheck,
			String requestId, 
			String dsAId, 
			String FHEModel, 
			String urlToBack, 
			boolean isMultithreading) throws IOException {
		return FheEncryptingField(			
				lstKeys,
				rootDir,
				baseDir,
				ipToCheck, 
				requestId, 
				dsAId, 
				FHEModel, urlToBack, false, false, null);
	}
	
	public static boolean FheEncryptingField(			
			KeyTransfer lstKeys,
			String rootDir,
			String baseDir,
			List<String> ipToCheck,
			String requestId, 
			String dsAId, 
			String FHEModel, 
			String urlToBack, 
			boolean isMultithreading) throws IOException {
		return FheEncryptingField(			
				lstKeys,
				rootDir,
				baseDir,
				ipToCheck, 
				requestId, 
				dsAId, 
				FHEModel, urlToBack, false, isMultithreading, null);
	}
	
	public static boolean FheEncryptingField(			
			KeyTransfer lstKeys,
			String rootDir,
			String baseWorkingDir,
			List<String> ipToCheck,  			
			String dsAId, 
			String requestId, 
			String FHEModel, 
			String urlToBack,
			boolean isNeedAnalysis, 
			boolean isMultithreading, 
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController) throws IOException {
		
		assert(lstKeys.getLstKeys().size() != 0);
		assert(ipToCheck.size() != 0);
		
					
		FileOperations.PrintData(baseWorkingDir + File.separator + Constants.FheKeys.FHE_PK_FILE, GetHEPublicKey(lstKeys.getLstKeys()));
		FileOperations.PrintData(baseWorkingDir + File.separator + Constants.FheKeys.FHE_EVAL_FILE, GetHEEvaluationKey(lstKeys.getLstKeys()));
		FileOperations.PrintData(baseWorkingDir + File.separator + Constants.FheKeys.FHE_PARAM_FILE, GetHEParameter(lstKeys.getLstKeys()));		
		log.info("[FHE-ENCRYPTION] configuration folder exec : " + Constants.FheScripts.FHE_CONFIG_EXEC + " " + baseWorkingDir );
		BashTools.Exec(true, Constants.FheScripts.FHE_CONFIG_EXEC, baseWorkingDir, FHEModel);
		/*Working with several IP to check*/					
		
		log.info("[FHE-ENCRYPTION] Starting exec Bash !");
		/*Starting encrypting several IP */
		if(isMultithreading)
			log.info("[FHE-ENCRYPTION] Don't supporting for multithreading !"); //Just disable comment below to make enable it
//			ApplicationDeployer.executor.execute(new EncryptIpServices(
//				rootDir, baseWorkingDir, dsAId, requestId, urlToBack, 
//				ipToCheck, isNeedAnalysis, fheBlacklistAnalyisiController));
		else 
			new EncryptIpServices(
					rootDir, baseWorkingDir, dsAId, requestId, FHEModel, urlToBack, 
					ipToCheck, isNeedAnalysis, fheBlacklistAnalyisiController).run();
		
		return true;
	}

	public static boolean CheckExistingDatabase(String baseDir) {		
		File file = new File(baseDir);
		if (file.exists()) {
			return true;
		}		
		return false;
	}	
	
    
	public static String encryptedIPs(
			FheKeyController fheKeyController,			
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController,
			String urlToBack, 
			String dsAId, 
			String requestId,			
			String fhEModel,
			List<String> ipToCheck, 
			boolean isNeedAnalysis) throws Exception 
	{
    	return encryptedIPs(
    			fheKeyController, 
    			fheBlacklistAnalyisiController,
    			urlToBack, dsAId, 
    			requestId, 
    			fhEModel, 
    			ipToCheck, isNeedAnalysis, 
    			true /*isUsingMulti-threading*/);
	}

	public static String encryptedIPs(
			FheKeyController fheKeyController,			
			FheBlacklistAnalysisRespository fheBlacklistAnalyisiController, 
			String urlToBack, 
			String dsAId, 
			String requestId, 
			String fhEModel, 
			List<String> ipToCheck, 
			boolean isNeedAnalysis,
			boolean isMultithreading) throws Exception 
	{
		fheKeyController.init(requestId, dsAId, fhEModel);  
		KeyTransfer lstKeys = __GetAndCreateFHEKeysIfNeed(fheKeyController, dsAId, fhEModel);
		boolean isExecuting = false;
		
		String rootDir = Constants.FHE_REPOSITORY_DIR; 
		String baseWorkingDir = String.format( Constants.FheAlgo.BLACKLIST.PREPAREDATA_DIR, 
												dsAId + File.separator + requestId, fhEModel); 					
		try {
			if(isNeedAnalysis == false)
				isExecuting = ToolControllers.FheEncryptingField(
						lstKeys, rootDir, baseWorkingDir,
						ipToCheck, dsAId, requestId, fhEModel, urlToBack, isMultithreading);
			else {							
				assert(fheBlacklistAnalyisiController != null);
				/*When using automatically invoking IAI/FHE-Analysis, 
				 * need to put clearly URL-Back for getting DPO-ID*/
				isExecuting = ToolControllers.FheEncryptingField(
						lstKeys, rootDir, baseWorkingDir,
						ipToCheck, dsAId, requestId, fhEModel, urlToBack, 
						true /*need analysis*/, isMultithreading,
						fheBlacklistAnalyisiController);				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String targetCiPhertextFolder = String.format(
				Constants.FheAlgo.BLACKLIST.ANALYSISDATA_DIR, dsAId + File.separator + requestId, fhEModel); 
		return isExecuting ? targetCiPhertextFolder : "";
	}

	private static KeyTransfer  __GetAndCreateFHEKeysIfNeed(FheKeyController fheKeyController, String dsaID, String fheModel) throws Exception {
		/*First Seeking for the 3 keys in storage folder */
		KeyTransfer lstKeys = null;
		String storageKeyData = String.format(Constants.FheAlgo.BLACKLIST.KEYDATA_DIR, dsaID, fheModel);	
		log.info("Check key at folder " + storageKeyData);
		lstKeys = fheKeyController.loadKeysFromLocalStorage(storageKeyData);
		
		if(lstKeys == null)
		{
			boolean isCreated = fheKeyController.CreatingFHEKeyIfNeed();
			log.info("No existed key at folder " + storageKeyData);

			if(isCreated)
			{
				/**
				 * Existed keys, process to analyse now
				 * */
				lstKeys = fheKeyController.getKeysForAnalysis();     
				for(KeyObject ko : lstKeys.getLstKeys())
				{
					log.info("Key " + ko.getKeyInfo().name() + " size = " + ko.getKey().length);
				}
				boolean isSave = fheKeyController.saveKeysInLocalStorage(lstKeys, storageKeyData);
				if(isSave == false)
					throw new Exception("Impossible to save key in local storage !");
			}
		}
		return lstKeys;		
	}

}
