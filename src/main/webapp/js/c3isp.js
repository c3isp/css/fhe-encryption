var c3isp = angular.module('c3isp', []);

c3isp.controller('TodoListController', function() {
	var todoList = this;
	todoList.todos = [
	  {text:'learn AngularJS', done:true},
	  {text:'build an AngularJS app', done:false}];
 
	todoList.addTodo = function() {
	  todoList.todos.push({text:todoList.todoText, done:false});
	  todoList.todoText = '';
	};
 
	todoList.remaining = function() {
	  var count = 0;
	  angular.forEach(todoList.todos, function(todo) {
		count += todo.done ? 0 : 1;
	  });
	  return count;
	};
 
	todoList.archive = function() {
	  var oldTodos = todoList.todos;
	  todoList.todos = [];
	  angular.forEach(oldTodos, function(todo) {
		if (!todo.done) todoList.todos.push(todo);
	  });
	};
}); 

c3isp.service('fileUpload', ['$https', function ($https) {
	this.uploadFileToUrl = function(file, uploadUrl) {
		 var fd = new FormData();
		 fd.append('file', file);
	
		 $https.post(uploadUrl, fd, {
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
		 })
	
		 .success(function() {
		 })
	
		 .error(function() {
		 });
	}
}]);

c3isp.controller('ExampleController', ['$scope', function($scope) {	
	$scope.submit = function(files) {		
		//var file = $scope.fileCEF;
		var file = files[0];
		console.log('file is ' );
		console.dir(file);
		console.log("here DSAID = " + this.dsaID + " filename " + file.name);
		var uploadUrl = 'http://10.8.35.48:8080/fhe-cingulata/v1/fhe/get-out-ip/dst';
	};
}]);

