var myApp = angular.module('myApp', []);      

myApp.service('fileUpload', ['$https', function ($https) {
   this.uploadFileToUrl = function(file, uploadUrl) {
      var fd = new FormData();
      fd.append('file', file);
   
      $https.post(uploadUrl, fd, {
         transformRequest: angular.identity,
         headers: {'Content-Type': undefined}
      })
   
      .success(function() {
      })
   
      .error(function() {
      });
   }
}]);

myApp.controller('myCtrl', ['$scope', 'fileUpload', function($scope, fileUpload) {
   $scope.submit = function() {
      var file = $scope.myFile;
      
      console.log('file is ' );
      console.dir(file);
      
    //   var uploadUrl = "/fileUpload";
    //   fileUpload.uploadFileToUrl(file, uploadUrl);
   };
}]);