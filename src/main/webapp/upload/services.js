(function () {
    'use strict';
    var myApp = angular.module('c3isp');
    
    myApp.service('sharedProperties', function () {        
        var modelAnalysis = 'BLACK_LIST_FULL';
        var dsaID = 'DSA-1f6fe910-f595-4c34-8b9c-837202db5f3a';
        var lstIPs = [    
                {text: '0.0.0.0', requestID:'', encrData:[
                    {fileName:'....', content:'...', postfix:'...'}
                ], done:-1}        
            ];
        return {
            getModelAnalysis: function () {
                return modelAnalysis;
            },
            setModelAnalysis: function(value) {
                modelAnalysis = value;
            },
            getDsaID: function () {
                return dsaID;
            },
            setDsaID: function(value) {
                dsaID = value;
            },
            getLstIPs: function () {
                return lstIPs;
            },
            setLstIPs: function(value) {
                lstIPs = value;
            }
        };
    });

    myApp.service('isiService', function ($http, $q) {
        this.getAnalysis = function (uploadUrl) {                        
                        
            var deffered = $q.defer();
            var request = {
                method: 'GET',
                url: uploadUrl
            };

            $http(request).then(function successCallback(response) {
                console.log(response);
                deffered.resolve(response);

            }, function errorCallback(response) {
                console.log(response);
                deffered.reject(response);
            });

            return deffered.promise;
        };
        
        this.analysisIP = function (uploadUrl) {                        
                        
            var deffered = $q.defer();
            var request = {
                method: 'POST',
                url: uploadUrl,                
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined, 
                    'Accept': 'application/json'
                }
            };

            $http(request).then(function successCallback(response) {
                console.log(response);
                deffered.resolve(response);

            }, function errorCallback(response) {
                console.log(response);
                deffered.reject(response);
            });

            return deffered.promise;
        };

        this.encryptFheIp = function (uploadUrl) {                        
                        
            var deffered = $q.defer();
            var request = {
                method: 'POST',
                url: uploadUrl,                
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined, 
                    'Accept': 'application/json'
                }
            };

            $http(request).then(function successCallback(response) {
                console.log(response);
                deffered.resolve(response);

            }, function errorCallback(response) {
                console.log(response);
                deffered.reject(response);
            });

            return deffered.promise;
        };

        this.uploadFileToUrl = function (file, uploadUrl) {
            //FormData, object of key/value pair for form fields and values
            var fileFormData = new FormData();
            fileFormData.append('cefFile', file);

            var deffered = $q.defer();
            var request = {
                method: 'POST',
                url: uploadUrl,
                data: fileFormData,
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined, 
                    'Accept': 'application/json'
                }
            };

            $http(request).then(function successCallback(response) {
                console.log(response);
                deffered.resolve(response);

            }, function errorCallback(response) {
                console.log(response);
                deffered.reject(response);
            });

            return deffered.promise;
        };
    });

    myApp.service('iaiService', function ($http, $q) {
        this.analysisIP = function (uploadUrl) {                        
                        
            var deffered = $q.defer();
            var request = {
                method: 'POST',
                url: uploadUrl,                
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined, 
                    'Accept': 'application/json'
                }
            };

            $http(request).then(function successCallback(response) {
                console.log(response);
                deffered.resolve(response);

            }, function errorCallback(response) {
                console.log(response);
                deffered.reject(response);
            });

            return deffered.promise;
        };

    });
})();