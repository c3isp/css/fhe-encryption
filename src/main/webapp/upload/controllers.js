(function () {
    'use strict';
    var myApp = angular.module('c3isp');


   // myApp.controller('ISIController', function ($scope, isiService, sharedProperties) {        
    myApp.controller('ISIController', function ($scope, $interval, $location, isiService) {        
        
        // $scope.IPs = sharedProperties.getLstIPs();
        // $scope.modelAnalysis = sharedProperties.getModelAnalysis();
        // $scope.dsaID = sharedProperties.getDsaID();           
        $scope.startTime = new Date();       
        $scope.currentIndex = -1;        
        $scope.timeToCheck = 1500;      //every 1.5s       
        $scope.modelAnalysis = 'BLACK_LIST_FULL';
        $scope.dsaID = '';
        $scope.IPs = [    
                {text: '0.0.0.0', requestID:'', encrData:[
                    {fileName:'....', content:'...', postfix:'...'}
                ], done:-99, canDo:0, performanceAnalysis:0, performanceEncrypt:0}        
            ];

        $scope.IPToAnalysis = {
                requestID:'', encrData:[
                    {fileName:'....', content:'...', postfix:'...'}
                ], done:-99, canDo:0, performanceAnalysis:0, performanceEncrypt:0};

        $scope.getResult = function () {  
            var uploadUrl = '/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}';
            uploadUrl = uploadUrl.replace("{RequestId}", $scope.IPToAnalysis.requestID);             
            uploadUrl = uploadUrl.replace("{DSAId}", $scope.dsaID); 
            uploadUrl = uploadUrl.replace("{FHEModel}", $scope.modelAnalysis); 
            var checkUrl = '/fhe-cingulata/v1/fhe/checking' + uploadUrl;
            var resultsUrl = '/fhe-cingulata/v1/fhe/results' + uploadUrl;

            console.log(new Date().toString() + "s :Check results : " + checkUrl);              
            var promise = isiService.getAnalysis(checkUrl); 
            promise.then(function (response) {
                var value = response.data.Value;
                console.log(new Date().toString() + "s :checkUrl server response : <" + value+">"); 
                if(value == "ok")
                {
                    $scope.IPToAnalysis.risk = 1; //ok finish analysis
                    var promiseRes = isiService.getAnalysis(resultsUrl); 
                    promiseRes.then(function (response) {
                        $interval.cancel($scope.intervalPromise);
                        console.log(new Date().toString() + "s :resultsUrl server response : " + response.data.Value);   
                        if(response.data.Value == "false")
                        {
                            $scope.IPs[$scope.currentIndex].risk = 0;
                        } 
                        if(response.data.Value == "true")
                        {
                            $scope.IPs[$scope.currentIndex].risk = 1;
                        }
                        $scope.IPs[$scope.currentIndex].performanceAnalysis = (new Date() - $scope.startTimes)/1000;
                        console.log("Total execution time " + $scope.IPs[$scope.currentIndex].performanceAnalysis + " s");
                    }, function () {
                        $scope.serverResponse = 'An error has occurred';
                    })
                }
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };


        $scope.checkingResult = function () {             
            $scope.intervalPromise = $interval(function(){ $scope.getResult(); } , $scope.timeToCheck);
        }

        $scope.uploadFile = function () {
            var file = $scope.cefFile;
            console.log("DSA = " + $scope.dsaID + " Model " + $scope.modelAnalysis);
            var uploadUrl = '/fhe-cingulata/v1/fhe/get-out-ip/dst';
            var promise = isiService.uploadFileToUrl(file, uploadUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
                var arrIp = $scope.serverResponse.data;
                $scope.IPs = [];
                angular.forEach(arrIp, function(element) {
                    $scope.IPs.push({text:element, requestID:'', encrData:[], done:-99, canDo:0}); 
                }); 
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };        

        $scope.encrypteIP = function(index) {            
                             
            // var regexPattern=new RegExp('/^\d+.\d+.\d+.\d+$/');
            // if(regexPattern.test(ip) == false)
            // {
            //     console.log("Error Sending IP format " + ip + " at " + index);    
            //     return; 
            // }
            // else         
            $scope.startTimes = new Date();
            $scope.currentIndex = index;     
            var ip = $scope.IPs[index].text;            
            console.log("Sending IP " + ip + " to encrypt ! at " + index);
            var uploadUrl = '/fhe-cingulata/v1/fhe/encrypt-ip/{ip}/{DSAId}/{FHEModel}';
            var uploadAdaptativeUrl = uploadUrl.replace("{ip}", ip);             
            uploadAdaptativeUrl = uploadAdaptativeUrl.replace("{DSAId}", $scope.dsaID); 
            uploadAdaptativeUrl = uploadAdaptativeUrl.replace("{FHEModel}", $scope.modelAnalysis);
            var promise = isiService.encryptFheIp(uploadAdaptativeUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
                var arrEncryptedData = $scope.serverResponse.data;
                $scope.IPs[index].requestID = arrEncryptedData.RequestID;
                angular.forEach(arrEncryptedData.ResultAnalyse, function(element) {
                    $scope.IPs[index].encrData.push({fileName:element.fileName,                                                     
                                                    content:element.content, 
                                                    postfix:'...'}); 
                });
                $scope.IPs[index].canDo = 1;
                /*Update IPs list*/
                //sharedProperties.setLstIPs($scope.IPs);
                $scope.IPs[index].performanceEncrypt = (new Date() - $scope.startTimes)/1000;
                console.log("Total execution time " + $scope.IPs[index].performanceEncrypt + " s");
                console.log("data #file = " + $scope.IPs[index].encrData.length);
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })
        };
        
        $scope.analysisIP = function (index) {
            $scope.currentIndex = index;            
            var requestID = $scope.IPs[index].requestID;
            var uploadUrl = '/RequestId/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}';
            uploadUrl = uploadUrl.replace("{RequestId}", requestID);             
            uploadUrl = uploadUrl.replace("{DSAId}", $scope.dsaID); 
            uploadUrl = uploadUrl.replace("{FHEModel}", $scope.modelAnalysis); 
            var uploadAdaptativeUrl = '/fhe-cingulata/v1/fhe/invoke-data-analysis' + uploadUrl;
            var currentUrl = $location.absUrl().split('/c3isp')[0]; //because the demo page in c3isp folder
            var urlToBack = currentUrl+'/v1/fhe/fhe-content-dpo' + uploadUrl;
            uploadAdaptativeUrl = uploadAdaptativeUrl + '?urlBack='+urlToBack;

            console.log("submit analysisIP with request : " + requestID + " for ip encrypted " 
                        + $scope.IPs[index].text + " with files:" + $scope.IPs[index].encrData.length
                        + " and send result back to " + urlToBack);
            
            $scope.IPs[index].risk          = -1; //processing now
            $scope.IPToAnalysis.requestID   = requestID;             
            $scope.IPToAnalysis.encrData    = $scope.IPs[index].encrData;
            $scope.IPToAnalysis.risk        = 0;                      
            $scope.startTimes               = new Date();
            var promise = isiService.analysisIP(uploadAdaptativeUrl);
            console.log("Waiting server response ");              
            promise.then(function (response) {
                $scope.serverResponse = response;
                console.log("server response : " + response.data.Response);                              
                
                $scope.checkingResult();                
            }, function () {
                console.log("NO server response : ");              
                $scope.serverResponse = 'An error has occurred';
            })
        };                

    });

    myApp.controller('IAIAnalysisController', function ($scope, iaiService, sharedProperties) {
        $scope.IPs = sharedProperties.getLstIPs();
        $scope.modelAnalysis = sharedProperties.getModelAnalysis();
        $scope.dsaID = sharedProperties.getDsaID();
        
        $scope.submitAnalysis = function($data) {            
            console.log("submit submitAnalysis");
        };

        $scope.analysisIP = function (index) {            
            var requestID = $scope.IPs[index].requestID;
            var uploadUrl = '/RequestId/{RequestId}/dsa/{DSAId}/scheme/{FHEModel}';
            uploadUrl = uploadUrl.replace("{RequestId}", requestID);             
            uploadUrl = uploadUrl.replace("{DSAId}", $scope.dsaID); 
            uploadUrl = uploadUrl.replace("{FHEModel}", $scope.modelAnalysis); 
            var uploadAdaptativeUrl = '/fhe-cingulata/v1/fhe/invoke-data-analysis' + uploadUrl;
            uploadAdaptativeUrl = uploadAdaptativeUrl + '?urlBack=http://10.8.34.27:8080/fhe-cingulata/v1/fhe/fhe-content-dpo' + uploadUrl;

            console.log("submit analysisIP " + requestID + " for ip encrypted " + $scope.IPs[index].ip + " with files:" + $scope.IPs[index].encrData.length);

            /*var promise = iaiService.analysisIP(uploadAdaptativeUrl);

            promise.then(function (response) {
                $scope.serverResponse = response;
                var arrIp = $scope.serverResponse.data;
                angular.forEach(arrIp, function(element) {
                    $scope.IPs.push({text:element, encrData:[], done:1, canDo:0}); 
                }); 
            }, function () {
                $scope.serverResponse = 'An error has occurred';
            })*/
        };  
    });

})();