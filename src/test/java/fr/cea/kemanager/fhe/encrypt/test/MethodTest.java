package fr.cea.kemanager.fhe.encrypt.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponseSupport;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.fhe.encrypt.helper.Constants;
import fr.cea.kemanager.fhe.encrypt.helper.Tools;
import fr.cea.kemanager.fhe.encrypt.helper.ToolsFolder;
import fr.cea.kemanager.fhe.encrypt.models.ResultEncryptedData;
import fr.cea.kemanager.fhe.encrypt.restapi.FheCingulataApi;
import fr.cea.kemanager.lib.DataObject;
import fr.cea.kemanager.vault.common.VaultStart;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MethodTest {
	Logger log = LoggerFactory.getLogger(MethodTest.class);
	

    @Value("${token.c3isp.cnr.it}")
    String TokenRoot;
    
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
	
	@Test
	public void testCompareHashFile() {
		System.out.println("testCompareHashFile ... ");
//		String dsAId = "DSA-56976731-3c16-46cc-a4e1-8384c6208eb0";
//
//        String data="CEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.2 spt=24920 dst=2.4.55.66 dpt=22126 proto=UDP end=1505462160000 dtz=Europe/Berlin \r\n" + 
//        		"CEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.3 spt=22126 dst=103.13.29.158 dpt=24920 proto=TCP end=1505462161000 dtz=Europe/Berlin";
//        
//        DataObject fileData = new DataObject()
//        		.checksum("checkSume")
//        		.dataContent(data).dataName("dataName")
//        		.urlToBack("urlToBack")
//				.putOtherPropertiesItem("offsetTranscrypting", "0 1 2 3 4")
//				.putOtherPropertiesItem("encryptedField", "dst");
//          
//        String localVarPath = "/v1/transcryption/encrypt/CEA_REsquest/dsa/{DSAId}/scheme/BLACK_LIST"
//        		.replaceAll("\\{" + "DSAId" + "\\}", dsAId);
//        String rootDir = "/home/nguyen/DEV/CEA/H2020/C3ISP/repository"; 
//        String baseDir = rootDir + "/prepareData/" + dsAId;
//        String requestId = "CEA_REsquest";
//        String FHEModel = "BLACK_LIST";
//        List<String> ipToCheck = Tools.ExtractValueFromFieldText(fileData.getDataContent(), "dst");
//        System.out.println("List ipToCheck IP = " + ipToCheck.size());
//        System.out.println("Object to send \n " + asJsonString(fileData));
       // DataObject serverResponse = ToolControllers.FheEncryptingField(log, null, rootDir, baseDir, fileData, requestId, dsAId, FHEModel);    
	}	
	
	@Test
	public void testReadCiphertext()
	{
		String storage = "/opt/FHE_CIN/C3ISP/analysisData/DSA-1545055495610-8ee6021a-1775-4bc5-96fe-4faca0b1966a/8e58d2b8-80a8-49a9-864f-c43855758bba/BLACK_LIST/IP_0/";
		System.out.println("looking for "  + storage);
		//ResultEncryptedData resData = ToolsFolder.getAllContentFiles(storage);
//		ResultEncryptedData resData = ToolsFolder.getPartialContentFilesInFolder(storage);		
//		System.out.println("Containg #files = " + resData.getResultAnalyse().size());
//		System.out.println("Object to send \n " + asJsonString(resData));
//		
//		assert(resData.getResultAnalyse().size() > 0);
	}
	
}
