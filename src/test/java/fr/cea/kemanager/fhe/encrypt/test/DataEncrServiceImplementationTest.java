/**
 *  Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package fr.cea.kemanager.fhe.encrypt.test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cea.kemanager.fhe.encrypt.ApplicationDeployer;
import fr.cea.kemanager.fhe.encrypt.helper.ToolsFolder;
import fr.cea.kemanager.fhe.encrypt.repository.FheBlacklistAnalysisRespository;
import fr.cea.kemanager.fhe.encrypt.restapi.FheCingulataApiImpl;
import fr.cea.kemanager.fhe.encrypt.vault.io.FileOperations;
import fr.cea.kemanager.lib.ObjectParameterRequest;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes=ApplicationDeployer.class)
@RunWith(SpringRunner.class)
//@WebMvcTest(DataApiController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test") // load application-test.properties
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataEncrServiceImplementationTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
	private FheBlacklistAnalysisRespository fheBlacklistAnalyisiController;
	
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;

    public String escapeString(String str) {
        try {
            return URLEncoder.encode(str, "utf8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }
    
    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
//	@Test
//    public void test01get() throws Exception {
//        
//        String param = "test";
//        String expectedOutput = "aName";
//        String Id = "DSA-56976731-3c16-46cc-a4e1-8384c6208eb0";
//
//        String data="CEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.2 spt=24920 dst=2.4.55.66 dpt=22126 proto=UDP end=1505462160000 dtz=Europe/Berlin\r\n" + 
//        		"CEF:0|Router_Vendor|Router_CED|1.0|100|Connection Detected|5|src=192.168.1.3 spt=22126 dst=103.13.29.158 dpt=24920 proto=TCP end=1505462161000 dtz=Europe/Berlin";
//        
//        ObjectParameterRequest objRequest = new ObjectParameterRequest()
//				.id(Id)
//				.putParamPropertiesItem("fileContent", data)
//				.putParamPropertiesItem("encryptedField", "dst");
//         
//        System.out.println(">>>>>>>>>>>"+restUser);
//        
//        String localVarPath = "/v1/transcryption/encrypt/CEA_REsquest/dsa/{DSAId}/scheme/BLACK_LIST"
//        		.replaceAll("\\{" + "DSAId" + "\\}", escapeString(Id.toString()));
//       
//    
//    }

	@Test
    public void testGetChildFolders() throws Exception {
		System.out.println("<<<<<<<<<<<< Runninig test get Child folders " );
        String param = "test";
        fheBlacklistAnalyisiController.init("requestId", "DSA-ID", "Model");
        System.out.println("Test init fheBlacklistAnalyisiController " + restUser + " througout : " + fheBlacklistAnalyisiController.getFullUrl());
        String parentFolder = "/home/nguyen/Documents/SAMPLE_DPO_metadata/test";
        File parentFile = new File(parentFolder); 
        
        if(parentFile.exists() == false)
        {
        	System.out.println("Local test folder is END !");
        	return;
        }	
        //File parentfolder = new
        List<String> childsFolder = ToolsFolder.getAllChildFolderInFolderParent(parentFolder);
        System.out.println(">>>>>>>>>>> Child folders = " + childsFolder.size());
        assertTrue(childsFolder.size() > 0);
        
        //test for creating zip
        String requestId = UUID.randomUUID().toString();
        String vdlPath = "/home/nguyen/Documents/SAMPLE_DPO_metadata/testZip";
        String vdlDestinationPath = vdlPath + File.separator + requestId; 
        Path destinationFolderPath = Paths.get(ToolsFolder.CreateAllFolderNames(vdlDestinationPath));
        System.out.println("<<<<<<<<<<<<" + destinationFolderPath.toAbsolutePath()); 
        ArrayList<String> arrZipIps = new ArrayList<>();
        int i = 0;
        for (String folderIP : childsFolder) {			
			String pathToipZipFile = ToolsFolder.zipAllFilesInFolder(folderIP, requestId+"_"+i, destinationFolderPath);
			System.out.println("zip files in folder " + folderIP + " = " + pathToipZipFile);
			arrZipIps.add(pathToipZipFile);
			++i;
		}
        System.out.println("arrZipFiles size " + arrZipIps.size());
        assertTrue(arrZipIps.size() > 0);
    }
}
